import sys

""" 
Read prediction for WMT15 test data from QUETCH or VW output and put in submission format
Submission format:
<METHOD NAME> <SEGMENT NUMBER> <WORD INDEX> <WORD> <BINARY SCORE> 
separator: single tab
"""

def formatQuetch(outputFile, testFile, submissionFile, methodName):
	o = open(outputFile, "r")
	t = open(testFile, "r")
	s = open(submissionFile, "w")

	testlines = t.readlines()
	
	for i,line in enumerate(o): 
	#format: 10.0    0       El      OK      OK      OK
		#print line
		word_test = testlines[i].split()[2]
		(segmentNumbPoint, wordIndex, word, multi, l1, binary) = line.strip().split()
		segmentNumber = int(float(segmentNumbPoint))
		#if binary == "OK":
		#	binary = "GOOD"

		s.write("%s\t%s\t%s\t%s\t%s\n" % (methodName, segmentNumber, wordIndex, word_test, binary))	

	o.close()
	t.close()
	s.close()

def formatVW(outputFile, testFile, submissionFile, methodName):
	o = open(outputFile, "r")
        t = open(testFile, "r")
        s = open(submissionFile, "w")

	preds = [float(line.strip()) for line in o]
	preds = ["OK" if pred>0 else "BAD" for pred in preds]

        #format: -0.177694

	#print preds
	
	#read testfile
	#format: 0	0	?	OK

	tokenCount = 0
	for line in t:
		splitted = line.split()
		word = splitted[2]
		segmentNumber = splitted[0]
		wordIndex = splitted[1]
		binary = preds[tokenCount]
		s.write("%s\t%s\t%s\t%s\t%s\n" % (methodName, segmentNumber, wordIndex, word, binary))
		tokenCount += 1



if __name__ == "__main__":

	testFile = "../WMT15-data/task2_en-es_test_comb/test.target.comb"
	#testFile = "../WMT15-data/task2_en-es_dev_comb/dev.target.comb"

	outputFile = sys.argv[1]
	submissionFile = sys.argv[2]
	methodName = sys.argv[3]

	print "Reading test data from", testFile
	print "Reading prediction data from", outputFile
	print "Writing output to", submissionFile	

	if outputFile.endswith(".results"): #QUETCH 
		formatQuetch(outputFile, testFile, submissionFile, methodName)
	elif outputFile.endswith(".out"): #VW
		formatVW(outputFile, testFile, submissionFile, methodName)
	else:	
		print "Couldn't decide whether QUETCH or VW output given"
