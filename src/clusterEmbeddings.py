#-*- coding: utf-8 -*-

#cluster word embeddings

import numpy as np
from sklearn.cluster import KMeans
from sklearn.cluster import DBSCAN
from sklearn.cluster import AgglomerativeClustering
from sklearn.decomposition import TruncatedSVD
from ParameterInspection import dataToDicts15
from gensim import models, corpora
from EvalModel import loadParams
from matplotlib import pyplot as plt
from scipy.spatial.distance import cosine

def getKey(item):
    return item[1]

if __name__=="__main__":

    words = []
    words = u"banana plátano fruit fruta apple manzana love amor loves amar hate odio hates odiar i yo you tú and y but pero ? ¿ buy comprar purchase compra big grande large gran person persona human humano people gente cat gato dog perro tortoise tortuga was estaba it está is es".split()
    language = ['0.0', '1', '0.0', '1', '0.0', '1','0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1']

    modelDict = {"submitted":"../parameters/2015-06-06--11:50:46.512444.15.params", "late": "../parameters/2015-06-26--13:48:39.961161.345.params", "vanilla":"../parameters/2015-06-06--11:42:27.119929.323.params", "word2vec":"../parameters/2015-06-06--11:50:46.512444.0.params"}

    for model, param in modelDict.items():
        print "\nLoading sims for model", model
        #for QUETCH model   
        #parameterFile = "../parameters/2015-06-06--11:50:46.512444.55.params"
        #parameterFile = "../parameters/2015-06-06--11:49:22.916009.200.params"
        #../dicts/2015-07-28--08:48:39.880538.dict
        #parameterFile = "../parameters/2015-07-28--08:48:43.132513.5.params" #128dims
        parameterFile = param
        lt = loadParams(parameterFile)[0].get_value()
        wd = dataToDicts15()

        #print wd
        wordlist = list()
        words = wd.values() #51074 words
        for w in words: 
            iw = wd.token2id[w]
            #print w, iw, lt[iw]
            wordlist.append(lt[iw])
        
        k = 30
        X = wordlist
        Y = language
       
        kmeans = KMeans(n_clusters=k)
        kmeans.fit(X)
        klabels = kmeans.labels_
        kinertia = kmeans.inertia_
        print "Inertia:", kinertia
        kclusters = dict()
        for w,l in zip(words,klabels):
            points = kclusters.setdefault(l,list())
            points.append(w)
        for l,c in kclusters.items():
            #get the words closest to centroid
            top10 = list()
            maxdist = np.inf
            pointsDistances = list()
            centroid = np.zeros(10)
            i = 0
            for w in c:
                iw = wd.token2id[w] 
                embedding = lt[iw] #get representation
                centroid += embedding
                i+=1
            centroid = centroid/i
            for w in c:    
                iw = wd.token2id[w] 
                embedding = lt[iw] #get representation
                dist = np.sum([x**2 for x in np.abs(embedding-centroid)])
                pointsDistances.append([w,dist])
            top10 = sorted(pointsDistances, key=getKey)[:10] 
                        
            print ", ".join([w for w,d in top10])
            
        #dbscan = DBSCAN(min_samples=2,eps=0.5)
        #dbscan.fit(X)
        #dlabels = dbscan.labels_
        #dclusters = dict()
        #for w,l in zip(words,dlabels):
            #points = dclusters.setdefault(l,list())
            #points.append(w)

        #for l,c in dclusters.items():
            #get the words closest to centroid
            #top10 = list()
            #maxdist = np.inf
            #pointsDistances = list()
            #centroid = np.zeros(10)
            #i = 0
            #for w in c:
                #iw = wd.token2id[w] 
                #embedding = lt[iw] #get representation
                #centroid += embedding
                #i+=1
            #centroid = centroid/i
            #for w in c:    
                #iw = wd.token2id[w] 
                #embedding = lt[iw] #get representation
                #dist = np.sum([x**2 for x in np.abs(embedding-centroid)])
                #pointsDistances.append([w,dist])
            #top10 = sorted(pointsDistances, key=getKey)[:10] 
                        
            #print ", ".join([w for w,d in top10])
       
        #agglo = AgglomerativeClustering(n_clusters=k, affinity='cosine', linkage='average')
        #agglo.fit(X)
        #alabels = agglo.labels_
        #aclusters = dict()
        #for w,l in zip(words,alabels):
            #points = aclusters.setdefault(l,list())
            #points.append(w)
        #for l,c in aclusters.items():
            #print l
            #print ", ".join(c[:20])