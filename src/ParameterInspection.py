# -*- coding: UTF-8 -*-
from ContextExtractor import corpus2dict, corpus2dict15
import numpy as np
from EvalModel import loadParams
import cPickle
from operator import itemgetter
import sys
from gensim import models, corpora
import argparse
"""
Inspect the lookup-table parameters:
For each word in the test and trainingsset for a specific task the lookup-table-layer trains a representation in a d_wrd-dimensional feature space.
Which words are most similar according to their representation in the learned feature space?
"""


def dataToDicts1(languagePair, trainPath, testPath, useFeatures=False):
	"""build mappings from words to indices for task 1"""
	#load train and test data (and optional features for task 1.1)
	test_source = testPath+"/"+languagePair+"_source.test"
	test_target = testPath+"/"+languagePair+"_target.test"
	train_source = trainPath+"/"+languagePair+"_source.train"
	train_target = trainPath+"/"+languagePair+"_target.train"
	data = [test_source, test_target, train_source, train_target]
	if useFeatures:
		train_features = trainPath+"/task1-1_"+languagePair+"_training.features"
		test_features = testPath+"/task1-1_"+languagePair+"_test.features"	
		data.extend([train_features, test_features])	
	#create the word dictionary (the creation of the word dictionary is deterministic) that maps words to indices
	wordDictionary = corpus2dict(data) 	
	return wordDictionary


def dataToDicts2(languagePair, trainPath, testPath):
	"""build mappings from words to indices for task 2"""
	#load train and test data
	test_source = testPath+"/"+languagePair+".source.test"
	test_target = testPath+"/"+languagePair+".tgt_ann.test"
	train_source = trainPath+"/"+languagePair+".source.train"
	train_target = trainPath+"/"+languagePair+".tgt_ann.train"
	data = [test_source, test_target, train_source, train_target]
	#create the word dictionary (the creation of the word dictionary is deterministic) that maps words to indices
	wordDictionary = corpus2dict(data) 	
	return wordDictionary

def dataToDicts14(languagePair, lowercase=True):
	""" build mappings from words to indices for 14 task 2 in new format"""
	#load train, test data
	lc = ".lc" if lowercase else ""
	languagePairUpper = languagePair.upper().replace("-","_")
	train_source = "../WMT14-data/task2_"+languagePair+"_train_comb/"+languagePairUpper+".source.train.tok"+lc+".comb"
	test_source = "../WMT14-data/task2_"+languagePair+"_test_comb/"+languagePairUpper+".source.test.tok"+lc+".comb"
	train_target = "../WMT14-data/task2_"+languagePair+"_train_comb/"+languagePairUpper+".tgt_ann.train"+lc+".comb"
	test_target = "../WMT14-data/task2_"+languagePair+"_test_comb/"+languagePairUpper+".tgt_ann.test"+lc+".comb"

        if lowercase:
                print "... loading lowercase data"
        else:
                print "... loading truecase data"

        data = [test_source, test_target, train_source, train_target]
        wordDictionary = corpus2dict15(data)
        return wordDictionary


def dataToDicts15(lowercase=True, d=None):
	""" build mappings from words to indices for 15 task 2"""
	#load train, dev, test data
	lc = ".lc" if lowercase else ""
	train_source = "../WMT15-data/task2_en-es_train_comb/train.source"+lc+".comb" 
	dev_source = "../WMT15-data/task2_en-es_dev_comb/dev.source"+lc+".comb"
	test_source = "../WMT15-data/task2_en-es_test_comb/test.source"+lc+".comb"
	train_target = "../WMT15-data/task2_en-es_train_comb/train.target"+lc+".comb"
	dev_target = "../WMT15-data/task2_en-es_dev_comb/dev.target"+lc+".comb"
	test_target = "../WMT15-data/task2_en-es_test_comb/test.target"+lc+".comb"

        if lowercase:
        	print "... loading lowercase data"
        else:
         	print "... loading truecase data"

	if d is not None:
		print "... loading dictionary from file", d
		wordDictionary = corpora.dictionary.Dictionary.load(d)
	else:
        	#print self.test_source, self.test_target, self.train_source, self.train_target, self.testtest_source, self.testtest_target
		data = [dev_source, dev_target, train_source, train_target, test_source, test_target]
		wordDictionary = corpus2dict15(data)
	return wordDictionary


def cosineSim(a,b):
	"""calculate the cosine similarity for a given pair of vectors"""
	dot = np.dot(a,b)
	anorm = np.linalg.norm(a)
	bnorm = np.linalg.norm(b)
	cos = dot/(anorm*bnorm)
	return cos
	
def euclideanSim(a,b):
	return 1/(1.+np.linalg.norm(a-b))

def getTopKSimilarWord2Vec(model,query,k,wd,metric):
	closestPairs = list()
	i = wd.token2id[query]
	v = model[query] #vector
	for w in wd.values():
		#print w
		j = wd.token2id[w]
		try:
			av = model[unicode(w)]
		except KeyError:
			continue		

		if metric=="cos": #cosine similarity
                        cs = cosineSim(v,av)
                elif metric=="eucl": #euclidean similarity
                        cs = 1/(1.+np.linalg.norm(v-av))
                if w==query:
                        continue
		if len(closestPairs) < k: #list is not filled yet
                        closestPairs.append((cs,(i,j))) #add to closest list
                        closestPairs = sorted(closestPairs, key=itemgetter(0), reverse=True) #sort list
                else: #list contains at least k entries
                        if cs > closestPairs[-1][0]: #if higher similarity than last entry in list
                                closestPairs.append((cs,(i,j))) #add to closest list 
                                closestPairs = sorted(closestPairs, key=itemgetter(0), reverse=True) #sort list
                                closestPairs = closestPairs[:k] #crop list
        return closestPairs     

		
def getTopKSimilarRows(lt,i,k,metric): #i: index of row in lt
	"""get most similar rows to given row"""
	row = lt[i]
	closestPairs = list()
	#find k most similar other rows
	for j,arow in enumerate(lt):
		if metric=="cos": #cosine similarity
			cs = cosineSim(row,arow)
		elif metric=="eucl": #euclidean similarity
			cs = 1/(1.+np.linalg.norm(row-arow))
		if j==i:
			continue
		if len(closestPairs) < k: #list is not filled yet
			closestPairs.append((cs,(i,j))) #add to closest list
			closestPairs = sorted(closestPairs, key=itemgetter(0), reverse=True) #sort list
		else: #list contains at least k entries
			if cs > closestPairs[-1][0]: #if higher similarity than last entry in list
				closestPairs.append((cs,(i,j))) #add to closest list
				closestPairs = sorted(closestPairs, key=itemgetter(0), reverse=True) #sort list
				closestPairs = closestPairs[:k] #crop list
	return closestPairs

if __name__=="__main__":

	#========== TASK 2 =======================================================
	validPairs = ["all", "en-es", "es-en", "de-en", "en-de"]

	parser = argparse.ArgumentParser(description="Take a look at QUETCH trained parameters at word-level")
        parser.add_argument("WMTxx", type=int, choices=range(14,16), help="Which year's WMT QE task")
        parser.add_argument("LanguagePair", type=str, choices=validPairs, help="Source and target language")
        parser.add_argument("-wiki", "--WikiCorpusComparison", action="store_true", help="Compare with word2vec trained on Wikicorpus; default=False")
        parser.add_argument("-multi", "--MultilingualCorpusComparison", action="store_true", help="Compare with word2vec trained on multilingual parallel data; default=False")
	parser.add_argument("-wmt15", "--WMT15Corpus", action="store_true", help="Compare with word2vec trained on WMT15 data; default=False")
	parser.add_argument("-k", "--K", type=int, help="Get top k most similar words to query")
	parser.add_argument("-tc", "--TrueCase", action="store_true", help="Use truecase words; default=False")
	parser.add_argument("ParameterFile", type=str, help="Path to parameters of trained model")
	parser.add_argument("Query", type=str, help="Get most similar words to query")
	parser.add_argument("-dict", "--Dictionary", type=str, help="Path to dictionary")
	parser.add_argument("-parallel", "--ParallelDataModel", action="store_true", help="Compare with word2vec trained on parallel data; default=False")
	parser.add_argument("-sim", "--SimilarityWithQuery", type=str, help="Get similarity between this word and the query")

	args = parser.parse_args()
        year = args.WMTxx
        languagePair = args.LanguagePair
	wiki = args.WikiCorpusComparison
	multi = args.MultilingualCorpusComparison
	wmt15 = args.WMT15Corpus
	k = args.K if args.K else 10
	query = unicode(args.Query)
	lowercase = not args.TrueCase
	parameterFile = args.ParameterFile
	lc = "lc." if lowercase else ""
	d = args.Dictionary
	parallel = args.ParallelDataModel
	query2 = args.SimilarityWithQuery

	if year == 15:
		wd = dataToDicts15(d=d)
	elif year == 14:
		wd = dataToDicts14(languagePair)

	lt_final = loadParams(parameterFile)[0].get_value()

	#print "Lookup table:", lt_final, "size: ", lt_final.shape
        #print "Dictionary size: ",len(wd)
	#print wd.token2id.keys() 

	if multi:
		#get word2vec similarities
		modelName = "features/multiling-parallelcorpora.en-es.word2vec.w5.d10."+lc+"model"
        	model = models.Word2Vec.load(modelName)
        	word2vecsims = model.most_similar(positive=[query], topn=k)

	#print word2vec sims
		print "\nTop %d similar Word2Vec words to '%s' with model %s:" % (k, query, modelName)
		for w,p in word2vecsims:
			print unicode(w)	
 		
		print "\nTop %d similar Word2Vec WMT15 words (filtered from whole list) to '%s' with model %s:" % (k, query, modelName)
                similarWord2Vecs_cos =  getTopKSimilarWord2Vec(model,query,k,wd,"cos")
                for (s,(i,j)) in similarWord2Vecs_cos:
                        print wd.get(j), s


	
	if wiki:
		#for wiki word2vec model
		modelName2 = "features/wiki.en-es.word2vec.w5.d10."+lc+"model"
		model2 =  models.Word2Vec.load(modelName2)
		word2vecsims = model2.most_similar(positive=[query], topn=k)

        	#print word2vec sims
        	print "\nTop %d similar Word2Vec words to '%s' with model %s:" % (k, query, modelName2)
        	for w,p in word2vecsims:
                	print unicode(w)
	
		print "\nTop %d similar Word2Vec WMT15 words (filtered from whole list) to '%s' with model %s:" % (k, query, modelName2)
                similarWord2Vecs_cos =  getTopKSimilarWord2Vec(model2,query,k,wd,"cos")
                for (s,(i,j)) in similarWord2Vecs_cos:
                        print wd.get(j), s


        if wmt15:
                #for wmt15 word2vec model
                modelName3 = "features/WMT15.en-es.word2vec.w5.d10.lc.model"
                model3 =  models.Word2Vec.load(modelName3)
                word2vecsims = model3.most_similar(positive=[query], topn=k)

                #print word2vec sims
                print "\nTop %d similar Word2Vec words to '%s' with model %s:" % (k, query, modelName3)
                for w,p in word2vecsims:
                        print unicode(w)
		
		print "\nTop %d similar Word2Vec WMT15 words (filtered from whole list) to '%s' with model %s:" % (k, query, modelName3)
                similarWord2Vecs_cos =  getTopKSimilarWord2Vec(model3,query,k,wd,"cos")
                for (s,(i,j)) in similarWord2Vecs_cos:
                        print wd.get(j), s 


	if parallel:
		#for word2vec model on parallel data
		modelName4 = "features/parallelcorpora.en-es.word2vec.w5.d10.lc.model"
                model4 =  models.Word2Vec.load(modelName4)
		model4.init_sims() #precompute L2-normalized vectors
                word2vecsims = model4.most_similar(positive=[query], topn=k)

                #print word2vec sims
                print "\nTop %d similar Word2Vec words to '%s' with model %s:" % (k, query, modelName4)
                for w,p in word2vecsims:
                        print unicode(w)

		print "\nTop %d similar Word2Vec WMT15 words (filtered from whole list) to '%s' with model %s:" % (k, query, modelName4)	
		similarWord2Vecs_cos =  getTopKSimilarWord2Vec(model4,query,k,wd,"cos")
                for (s,(i,j)) in similarWord2Vecs_cos:
                        print wd.get(j), s
	
				

	 #find the most similar rows in the table and their indices
        #similarity measure: cosine
        #check that lookup table and dictionary fit together
        if len(wd) != lt_final.shape[0]: #rows have to agree
                print "ERROR: lookup table and dictionary do not have the same number of entries."
                exit(-1)
        
        i = wd.token2id[query]
        similarIndices_cos = getTopKSimilarRows(lt_final,i,k,"cos")
	similarIndices_eucl = getTopKSimilarRows(lt_final,i,k,"eucl")

        #translate from indices to words to get the top similar words
        print "\nTop QUETCH pretrained %d cosine-similar words to '%s':" % (k,query)
        for (s,(i,j)) in similarIndices_cos:
                print wd.get(j), s

	print "\nTop QUETCH pretrained %d euclidean-similar words to '%s':" % (k,query)
        for (s,(i,j)) in similarIndices_eucl:
                print wd.get(j), s

	if query2:
		print "\nComputing similarity for word pair '%s'-'%s' in QUETCH model:" % (query, query2)
		p = wd.token2id[query]
		q = wd.token2id[query2]
		cos = cosineSim(lt_final[p], lt_final[q])
		eucl = euclideanSim(lt_final[p], lt_final[q])
		print "Cos sim %f; euclidean sim %f" % (cos, eucl)
	
		if wiki:
			print "\nSimilarities with word2vec pretrained on wiki:"
			cos = model2.similarity(query, query2)
			eucl = euclideanSim(model2[query], model2[query2])
			print "Cos sim %f; euclidean sim %f" % (cos, eucl)

			
	"""
	if parallel or multi: #TODO first only for parallel
		print "\nNow compare word2vec and QUETCH"
		print "\nTop word2vec %d cosine-similar words to '%s':" % (k,query)
		similarWord2Vecs_cos =  getTopKSimilarWord2Vec(model,query,k,wd,"cos")
		for (s,(i,j)) in similarWord2Vecs_cos:
			print wd.get(j)
	       	print "\nTop word2vec %d euclidean-similar words to '%s':" % (k,query)
		similarWord2Vecs_eucl = getTopKSimilarWord2Vec(model,query,k,wd,"eucl")
		for (s,(i,j)) in similarWord2Vecs_eucl:
			print wd.get(j)
			
		#get all similarities
		#filter the similarities
		#rank the filtered similarities
	"""
