import sys
from EvalModel import loadParams
import argparse
from gensim import models, corpora
from ParameterInspection import dataToDicts15, cosineSim, euclideanSim
from scipy import stats
import numpy as np
import unicodedata

#evaluate the vectors trained by QUETCH on the WordSim353 task

#parameters:
#-trained model parameters
#-NN properties

#load word sim task:
#word1 word2 sim

#load model
#load dict

#filter out samples that are not fully covered

#initialize NN

#use cosine sim
#and use euclidean sim
#and use dot product
#and more?
#to calculate vector similarity

#calculate pearson's correlation between predicted sim and gold sim
#print

def loadWordSim353Task(filePath,lc=True):
    inFile = open(filePath,"r")
    questions = list()
    isFirst = True
    #if lc:
    #   print "loading lowercase task triples"
    for line in inFile:
        if isFirst:
            isFirst = False
            continue
        (word1,word2,humanSim) = line.split()
        if lc:
            questions.append((word1.lower(),word2.lower(),float(humanSim)))
        else:
                        questions.append((word1,word2,humanSim))
    return questions

def loadSimLex999Task(filePath, lc=True):
    inFile = open(filePath,"r")
    questions = list()
    isFirst = True
    #if lc:
    #   print "loading lowercase task triples"
    for line in inFile:
        if isFirst:
            isFirst = False
            continue
        splitted = line.split()
        (word1, word2, humanSim) = (splitted[0],splitted[1],splitted[3])
        if lc:
            questions.append((word1.lower(),word2.lower(),float(humanSim)))
        else:
                        questions.append((word1,word2,humanSim))
    return questions

def loadRwTask(filePath, lc=True):
    inFile = open(filePath, "r")
    questions = list()
    #if lc:
    #   print "loading lowercase task triples"
    for line in inFile:
        splitted = line.split()
        (word1, word2, humanSim) = (splitted[0],splitted[1],splitted[2])
        if lc:
            questions.append((word1.lower(),word2.lower(),float(humanSim)))
        else:
                        questions.append((word1,word2,humanSim))
    return questions
    

def loadMCTask(filePath, lc=True):
    inFile = open(filePath, "r")
    questions = list()
    isFirst = True
    #if lc:
    #   print "loading lowercase task triples"
    for line in inFile:
        if isFirst:
            isFirst = False
            continue
        splitted = line.split()
        (word1, word2, humanSim) = (splitted[0],splitted[1],splitted[2])
        if lc:
            questions.append((word1.lower(),word2.lower(),float(humanSim)))
        else:
                        questions.append((word1,word2,humanSim))
    return questions
    
def loadRGTask(filePath, lc=True):
    inFile = open(filePath, "r")
    questions = list()
    isFirst = True
    #if lc:
    #   print "loading lowercase task triples"
    for line in inFile:
        if isFirst:
            isFirst = False
            continue
        splitted = line.split()
        (word1, word2, humanSim) = (splitted[0],splitted[1],splitted[2])
        if lc:
            questions.append((word1.lower(),word2.lower(),float(humanSim)))
        else:
                        questions.append((word1,word2,humanSim))
    return questions

def loadMENTask(filePath, lc=True):
    inFile = open(filePath, "r")
    questions = list()
    #if lc:
    #   print "loading lowercase task triples"
    for line in inFile:
        splitted = line.split()
        (word1, word2, humanSim) = (splitted[0],splitted[1],splitted[2])
        if lc:
            questions.append((word1.lower(),word2.lower(),float(humanSim)))
        else:
                        questions.append((word1,word2,humanSim))
    return questions

def loadMCESTask(filePath, lc=True):
    inFile = open(filePath, "r")
    questions = list()
    isFirst = True
    #if lc:
    #   print "loading lowercase task triples"
    for line in inFile:
        if isFirst:
            isFirst = False
            continue
        splitted = line.split(";")
        (word1, word2, humanSim) = (splitted[6].strip('"'),splitted[7].strip('"'),splitted[8])
        if lc:
            questions.append((word1.lower(),word2.lower(),float(humanSim)))
        else:
                        questions.append((word1,word2,humanSim))
        #print questions
    return questions
    
def loadMCESENTask(filePath, lc=True):
    inFile = open(filePath, "r")
    questions = list()
    isFirst = True
    #if lc:
    #   print "loading lowercase task triples"
    for line in inFile:
        if isFirst:
            isFirst = False
            continue
        splitted = line.split(";")
        (word1, word2, humanSim) = (splitted[0].strip('"'),splitted[7].strip('"'),splitted[8])
        if lc:
            questions.append((word1.lower(),word2.lower(),float(humanSim)))
        else:
                        questions.append((word1,word2,humanSim))
        #print questions
    return questions
    
def loadMCENESTask(filePath, lc=True):
    inFile = open(filePath, "r")
    questions = list()
    isFirst = True
    #if lc:
    #   print "loading lowercase task triples"
    for line in inFile:
        if isFirst:
            isFirst = False
            continue
        splitted = line.split(";")
        (word1, word2, humanSim) = (splitted[6].strip('"'),splitted[1].strip('"'),splitted[8])
        if lc:
            questions.append((word1.lower(),word2.lower(),float(humanSim)))
        else:
                        questions.append((word1,word2,humanSim))
        #print questions
    return questions
    
def loadWordSim353ESTask(filePath, lc=True):
    inFile = open(filePath, "r")
    questions = list()
    isFirst = True
    #if lc:
    #   print "loading lowercase task triples"
    for line in inFile:
        if isFirst:
            isFirst = False
            continue
        splitted = line.split(";")
        (word1, word2, humanSim) = (splitted[6].strip('"'),splitted[7].strip('"'),splitted[8])
        if lc:
            questions.append((word1.lower(),word2.lower(),float(humanSim)))
        else:
                        questions.append((word1,word2,humanSim))
        #print questions
    return questions

def loadWordSim353ENESTask(filePath, lc=True):
    inFile = open(filePath, "r")
    questions = list()
    isFirst = True
    #if lc:
    #   print "loading lowercase task triples"
    for line in inFile:
        if isFirst:
            isFirst = False
            continue
        splitted = line.split(";")
        (word1, word2, humanSim) = (splitted[0].strip('"'),splitted[7].strip('"'),splitted[8]) #first word: english, second word: spanish
        if lc:
            questions.append((word1.lower(),word2.lower(),float(humanSim)))
        else:
                        questions.append((word1,word2,humanSim))
        #print questions
    return questions
    
def loadWordSim353ESENTask(filePath, lc=True):
    inFile = open(filePath, "r")
    questions = list()
    isFirst = True
    #if lc:
    #   print "loading lowercase task triples"
    for line in inFile:
        if isFirst:
            isFirst = False
            continue
        splitted = line.split(";")
        (word1, word2, humanSim) = (splitted[6].strip('"'),splitted[1].strip('"'),splitted[8]) #first word: spanish, second word: english
        if lc:
            questions.append((word1.lower(),word2.lower(),float(humanSim)))
        else:
                        questions.append((word1,word2,humanSim))
        #print questions
    return questions

def strip_accents(s):
    return ''.join(c for c in unicodedata.normalize('NFD', s) if unicodedata.category(c) != 'Mn')


def getQUETCHvectorForWord(wd, lt, word):
    errorCode = 0
    try:
        i = wd.token2id[word]
    except KeyError:
        #print word, "not in dictionary"
        try:
            #for Spanish data sets, accents are missing on words
            for w in wd.values():
                if word==strip_accents(w):
                #print "word in gensim:", w
                    word = w
                #print "stripped accents:", strip_accents(w)
                    break
            i = wd.token2id[word] #try again
        except KeyError:
            #print "still didn't find it"
            i = wd.token2id["UNKNOWN"]
            errorCode = -1
    vector = lt[i]
    return vector, errorCode

def modelOnSimTask(triples, lt, wd, metric="cos"):
    predictedTriples = list()
    answeredOriginalTriples = list() #only those that the vocab covers
    for (word1, word2, humanSim) in triples:
        vectorWord1, errorCode1 = getQUETCHvectorForWord(wd, lt, word1)
        vectorWord2, errorCode2 = getQUETCHvectorForWord(wd, lt, word2)
        if errorCode1==0 and errorCode2==0: #both words are in dictionary
            if metric == "cos":
                sim = cosineSim(vectorWord1, vectorWord2)
            elif metric == "eucl":
                sim = euclideanSim(vectorWord1, vectorWord2)
            elif metric == "dot":
                sim = np.dot(vectorWord1, vectorWord2)
            #print word1, word2, humanSim, sim
            predictedTriples.append((word1, word2, sim))
            answeredOriginalTriples.append((word1, word2, humanSim))
    return predictedTriples, answeredOriginalTriples
    

def evalSimTask(lookupTable, wd, task, metric="cos", wiki=False, wmt15=False, wmt15wiki=False, lc="lc"):
    print "\n\nMetric:", metric

    if task == "ws353": #WordSim353 task
        triples = loadWordSim353Task("../SimSets/wordsims353/combined.tab")
        print "\nEvaluating QUETCH vectors on the WordSim353 task"
        #print len(triples), "triples loaded"
        print "-"*50
       
    elif task == "sl999": #SimLex999 task
        triples = loadSimLex999Task("../SimSets/SimLex-999/SimLex-999.txt")     
        print "\nEvaluating QUETCH vectors on the SimLex999 task"
        #print len(triples), "triples loaded"
        print "-"*50
        
    elif task == "RW": #RareWords task
        triples = loadRwTask("../SimSets/rw/rw.txt")
        print "\nEvaluating QUETCH vectors on the RareWords (rw) task"
        #print len(triples), "triples loaded"
        print "-"*50
    
    elif task == "MC": #MC task
        triples = loadMCTask("../SimSets/ConceptSim/MC_word.txt")
        print "\nEvaluating QUETCH vectors on the Miller and Charles (MC) task"
        #print len(triples), "triples loaded"
        print "-"*50
        
    elif task == "RG": #RG task
        triples = loadMCTask("../SimSets/ConceptSim/RG_word.txt")
        print "\nEvaluating QUETCH vectors on the Rubenstein and Goodenough (RG) task"
        #print len(triples), "triples loaded"
        print "-"*50
        
    elif task == "MEN": #MEN task
        triples = loadMENTask("../SimSets/MEN/MEN_dataset_natural_form_full")
        print "\nEvaluating QUETCH vectors on the MEN task"
        print "-"*50
    
    elif task == "MC-ES":
        triples = loadMCESTask("../SimSets/CLSR-EK/MC30.csv")
        print "\nEvaluating QUETCH vectors on the Spanish MC task"
        print "-"*50
        
    elif task == "ws353-ES":
        triples = loadWordSim353ESTask("../SimSets/CLSR-EK/WS353.csv")
        print "\nEvaluating QUETCH vectors on the Spanish WordSim task"
        print "-"*50
    
    elif task == "MC-ESEN":
        triples = loadMCESENTask("../SimSets/CLSR-EK/MC30.csv")
        print "\nEvaluating QUETCH vectors on the Spanish-English MC task"
        print "-"*50
        
    elif task == "ws353-ESEN":
        triples = loadWordSim353ESENTask("../SimSets/CLSR-EK/WS353.csv")
        print "\nEvaluating QUETCH vectors on the Spanish-English WordSim task"
        print "-"*50
    
    elif task == "MC-ENES":
        triples = loadMCENESTask("../SimSets/CLSR-EK/MC30.csv")
        print "\nEvaluating QUETCH vectors on the English-Spanish MC task"
        print "-"*50
        
    elif task == "ws353-ENES":
        triples = loadWordSim353ENESTask("../SimSets/CLSR-EK/WS353.csv")
        print "\nEvaluating QUETCH vectors on the English-Spanish WordSim task"
        print "-"*50
    
    QUETCH_modelTriples, QUETCH_answeredHumanTriples = modelOnSimTask(triples, lookupTable, wd, metric=metric) #might be a subset of tasks triples
    print "\n"+str(len(QUETCH_modelTriples))+"/"+str(len(triples)), "triples processed, coverage ", len(QUETCH_modelTriples)/float(len(triples))*100, "%"

    QUETCH_modelSimVector = [sim for (w1,w2,sim) in QUETCH_modelTriples]
    QUETCH_answeredHumanSimVector = [sim for (w1,w2,sim) in QUETCH_answeredHumanTriples]
    
    print "-"*50
    QUETCH_pearsonCorr = stats.pearsonr(QUETCH_modelSimVector, QUETCH_answeredHumanSimVector)
    print "QUETCH Pearson correlation", QUETCH_pearsonCorr[0]

    QUETCH_spearmanCorr = stats.spearmanr(QUETCH_modelSimVector, QUETCH_answeredHumanSimVector)
    print "QUETCH Spearman correlation", QUETCH_spearmanCorr[0]
    
    
    if wiki:
        modelName2 = "features/wiki.en-es.word2vec.w5.d10."+lc+"model"
        model2 =  models.Word2Vec.load(modelName2)
        
        #on all task triples
        wiki_full_modelTriples, wiki_full_answeredHumanTriples = word2vecOnSimTask(triples, model2, metric=metric)
        
        print "\n"+str(len(wiki_full_modelTriples))+"/"+str(len(triples)), "triples processed, coverage ", len(wiki_full_modelTriples)/float(len(triples))*100, "%"

        wiki_full_modelSimVector = [sim for (w1,w2,sim) in wiki_full_modelTriples]
        wiki_full_answeredHumanSimVector = [sim for (w1,w2,sim) in wiki_full_answeredHumanTriples]
        
        print "-"*50
        wiki_full_pearsonCorr = stats.pearsonr(wiki_full_modelSimVector, wiki_full_answeredHumanSimVector)
        print "Wiki Pearson correlation", wiki_full_pearsonCorr[0]

        wiki_full_spearmanCorr = stats.spearmanr(wiki_full_modelSimVector, wiki_full_answeredHumanSimVector)
        print "Wiki Spearman correlation", wiki_full_spearmanCorr[0]
        
        
        #only on triples that QUETCH covered
        wiki_reduced_modelTriples, wiki_reduced_answeredHumanTriples = word2vecOnSimTask(QUETCH_answeredHumanTriples, model2, metric=metric)
        
        print "\n"+str(len(wiki_reduced_modelTriples))+"/"+str(len(triples)), "triples processed, coverage ", len(wiki_reduced_modelTriples)/float(len(triples))*100, "%"

        wiki_reduced_modelSimVector = [sim for (w1,w2,sim) in wiki_reduced_modelTriples]
        wiki_reduced_answeredHumanSimVector = [sim for (w1,w2,sim) in wiki_reduced_answeredHumanTriples]
        
        print "-"*50
        wiki_reduced_pearsonCorr = stats.pearsonr(wiki_reduced_modelSimVector, wiki_reduced_answeredHumanSimVector)
        print "Wiki reduced Pearson correlation", wiki_reduced_pearsonCorr[0]

        wiki_reduced_spearmanCorr = stats.spearmanr(wiki_reduced_modelSimVector, wiki_reduced_answeredHumanSimVector)
        print "Wiki reduced Spearman correlation", wiki_reduced_spearmanCorr[0]

    if wmt15:
        modelName3 = "features/WMT15.en-es.word2vec.w5.d10."+lc+"model"
        model3 =  models.Word2Vec.load(modelName3)
        
        #on all task triples
        wmt15_modelTriples, wmt15_answeredHumanTriples = word2vecOnSimTask(triples, model3, metric=metric)
        
        print "\n"+str(len(wmt15_modelTriples))+"/"+str(len(triples)), "triples processed, coverage ", len(wmt15_modelTriples)/float(len(triples))*100, "%"

        wmt15_modelSimVector = [sim for (w1,w2,sim) in wmt15_modelTriples]
        wmt15_answeredHumanSimVector = [sim for (w1,w2,sim) in wmt15_answeredHumanTriples]
        
        print "-"*50
        wmt15_pearsonCorr = stats.pearsonr(wmt15_modelSimVector, wmt15_answeredHumanSimVector)
        print "WMT15 Pearson correlation", wmt15_pearsonCorr[0]

        wmt15_spearmanCorr = stats.spearmanr(wmt15_modelSimVector, wmt15_answeredHumanSimVector)
        print "WMT15 Spearman correlation", wmt15_spearmanCorr[0]

        #only on triples that QUETCH covered
        wmt15_reduced_modelTriples, wmt15_reduced_answeredHumanTriples = word2vecOnSimTask(QUETCH_answeredHumanTriples, model3, metric=metric)
        
        print "\n"+str(len(wmt15_reduced_modelTriples))+"/"+str(len(triples)), "triples processed, coverage ", len(wmt15_modelTriples)/float(len(triples))*100, "%"

        wmt15_reduced_modelSimVector = [sim for (w1,w2,sim) in wmt15_reduced_modelTriples]
        wmt15_reduced_answeredHumanSimVector = [sim for (w1,w2,sim) in wmt15_reduced_answeredHumanTriples]
        
        print "-"*50
        wmt15_reduced_pearsonCorr = stats.pearsonr(wmt15_reduced_modelSimVector, wmt15_reduced_answeredHumanSimVector)
        print "WMT15 reduced Pearson correlation", wmt15_reduced_pearsonCorr[0]

        wmt15_reduced_spearmanCorr = stats.spearmanr(wmt15_reduced_modelSimVector, wmt15_reduced_answeredHumanSimVector)
        print "WMT15 reduced Spearman correlation", wmt15_reduced_spearmanCorr[0]
        
    if wmt15wiki:
        modelName3 = "features/wiki+wmt15.en-es.word2vec.w5.d10."+lc+"model"
        model3 =  models.Word2Vec.load(modelName3)
        
        #on all task triples
        wmt15wiki_modelTriples, wmt15wiki_answeredHumanTriples = word2vecOnSimTask(triples, model3, metric=metric)
        
        print "\n"+str(len(wmt15wiki_modelTriples))+"/"+str(len(triples)), "triples processed, coverage ", len(wmt15wiki_modelTriples)/float(len(triples))*100, "%"

        wmt15wiki_modelSimVector = [sim for (w1,w2,sim) in wmt15wiki_modelTriples]
        wmt15wiki_answeredHumanSimVector = [sim for (w1,w2,sim) in wmt15wiki_answeredHumanTriples]
        
        print "-"*50
        wmt15wiki_pearsonCorr = stats.pearsonr(wmt15wiki_modelSimVector, wmt15wiki_answeredHumanSimVector)
        print "WMT15+Wiki Pearson correlation", wmt15wiki_pearsonCorr[0]

        wmt15wiki_spearmanCorr = stats.spearmanr(wmt15wiki_modelSimVector, wmt15wiki_answeredHumanSimVector)
        print "WMT15+Wiki Spearman correlation", wmt15wiki_spearmanCorr[0]

        #only on triples that QUETCH covered
        wmt15wiki_reduced_modelTriples, wmt15wiki_reduced_answeredHumanTriples = word2vecOnSimTask(QUETCH_answeredHumanTriples, model3, metric=metric)
        
        print str(len(wmt15wiki_reduced_modelTriples))+"/"+str(len(triples)), "triples processed, coverage ", len(wmt15wiki_reduced_modelTriples)/float(len(triples))*100, "%"

        wmt15wiki_reduced_modelSimVector = [sim for (w1,w2,sim) in wmt15wiki_reduced_modelTriples]
        wmt15wiki_reduced_answeredHumanSimVector = [sim for (w1,w2,sim) in wmt15wiki_reduced_answeredHumanTriples]
        
        print "-"*50
        wmt15wiki_reduced_pearsonCorr = stats.pearsonr(wmt15wiki_reduced_modelSimVector, wmt15wiki_reduced_answeredHumanSimVector)
        print "WMT15+Wiki reduced Pearson correlation", wmt15wiki_reduced_pearsonCorr[0]

        wmt15wiki_reduced_spearmanCorr = stats.spearmanr(wmt15wiki_reduced_modelSimVector, wmt15wiki_reduced_answeredHumanSimVector)
        print "WMT15+Wiki reduced Spearman correlation", wmt15wiki_reduced_spearmanCorr[0]
        
    #TODO print nice table with results

def word2vecOnSimTask(triples, model, metric="cos"):
    predictedTriples = list()
    answeredOriginalTriples = list() #only those that the vocab covers
    for (word1, word2, humanSim) in triples:
        vectorWord1 = None
        vectorWord2 = None
        try:
            vectorWord1 = model[word1]
        except KeyError:
            #print word, "not in dictionary"
            try:
                #for Spanish data sets, accents are missing on words
                for w in model.vocab.keys():
                    if word1==strip_accents(w):
                        print "word in gensim:", w
                        word1 = w
                        print "stripped accents:", strip_accents(w)
                        break
                    vectorWord1 = model[word1] #try again
            except KeyError:
                print "still didn't find it"

        try:
            vectorWord2 = model[word2]
        except KeyError:
        #print word, "not in dictionary"
            try:
                #for Spanish data sets, accents are missing on words
                for w in model.vocab.keys():
                    if word2==strip_accents(w):
                        print "word in gensim:", w
                        word2 = w
                        print "stripped accents:", strip_accents(w)
                        break
                    vectorWord2 = model[word2] #try again
            except KeyError:
                print "still didn't find it"
        
        if vectorWord1 is None or vectorWord2 is None:
            continue
        
        if metric == "cos":
            sim = cosineSim(vectorWord1, vectorWord2)
        elif metric == "eucl":
            sim = euclideanSim(vectorWord1, vectorWord2)
        elif metric == "dot":
            sim = np.dot(vectorWord1, vectorWord2)
        #print word1, word2, humanSim, sim
        predictedTriples.append((word1, word2, sim))
        answeredOriginalTriples.append((word1, word2, humanSim))
    return predictedTriples, answeredOriginalTriples
    
    
if __name__=="__main__":
    
    #settings:
    #1) QUETCH with whole WikiCorpus vocabulary in LTL, pretrained on WikiCorpus
    #python evalOnWordSimTasks.py ../parameters/2015-06-22--00:00:27.947786.4.params  -dict ../dicts/2015-06-22--00:00:16.604652.dict
    #2) QUETCH trained for a large number of iterations, pretrained on WikiCorpus, (a),(w),(p), standard best model
    #python evalOnWordSimTasks.py ../parameters/2015-06-26--13:48:39.961161.345.params -dict ../dicts/2015-06-26--13:48:32.130885.dict
    #early / best Acc: (badweight=1) ../parameters/2015-06-26--13:48:39.961161.0.params
    #best F1: (badweight=1) ../parameters/2015-06-26--13:48:39.961161.299.params
    #intermediate F1: (badweight=1) ../parameters/2015-06-26--13:48:39.961161.180.params
    #late: ../parameters/2015-06-26--13:48:39.961161.345.params
    #submitted: ../parameters/2015-06-06--11:50:46.512444.15.params
    #vanilla: ../parameters/2015-06-06--11:42:27.119929.323.params
#--------------------
#Accuracy: 0.738958072995
#--------------------
 #        REFERENCE
#PREDICT OK      BAD
#OK      15952   3255
#BAD     2803    1197
#--------------------
#OK: Precision 0.830530535742 Recall 0.850546520928
#BAD: Precision 0.29925 Recall 0.268867924528
#--------------------
#F1_OK: 0.840419366735
#F1_BAD: 0.283246568859
 #       NEW BEST!
  #      Saving parameters in ../parameters/2015-06-26--13:48:39.961161.299.params
#Saved parameters in  ../parameters/2015-06-26--13:48:39.961161.299.params

    #best Acc:
    #3) QUETCH vanilla

    parser = argparse.ArgumentParser(description="Evaluate QUETCH trained (WMT15) parameters on word similarity task")
    parser.add_argument("ParameterFile", type=str, help="Path to parameters of trained model")
    parser.add_argument("-dict", "--Dictionary", type=str, help="Path to dictionary")
    parser.add_argument("-wmt15", "--WMT15Corpus", action="store_true", help="Compare with word2vec trained on WMT15 data; default=False")
    parser.add_argument("-wmt15wiki", "--WMT15WikiCorpus", action="store_true", help="Compare with word2vec trained on Wikicorpus, further trained on WMT15 data; default=False")
    parser.add_argument("-wiki", "--WikiCorpusComparison", action="store_true", help="Compare with word2vec trained on Wikicorpus; default=False")
    parser.add_argument("-tc", "--TrueCase", action="store_true", help="Use truecase words; default=False")


    args = parser.parse_args()
    parameterFile = args.ParameterFile
    d = args.Dictionary
    wiki = args.WikiCorpusComparison
    wmt15 = args.WMT15Corpus
    wmt15wiki = args.WMT15WikiCorpus
    lowercase = not args.TrueCase
    lc = "lc." if lowercase else ""
    
    wd = dataToDicts15(d=d)
    print len(wd), "words in vocabulary loaded"
    
    lookupTable = loadParams(parameterFile)[0].get_value()
    print lookupTable.shape, "sized lookup table loaded"

    #evalWordSim353Task(lookupTable, wd, metric="eucl", wiki=wiki, wmt15=wmt15, lc=lc)
    #evalSimLex999Task(lookupTable, wd, metric="eucl", wiki=wiki, wmt15=wmt15, lc=lc)
    #evalRwTask(lookupTable, wd, metric="eucl", wiki=wiki, wmt15=wmt15, lc=lc)
    
    #run sim tasks
    
    #simTasks = ["MC-ESEN", "MC-ENES", "ws353-ESEN", "ws353-ENES"] #cross-lingual
    #simTasks = ["MC-ES", "ws353-ES"] #only spanish
    simTasks = ["MC-ES", "ws353-ES", "MC-ESEN", "MC-ENES", "ws353-ESEN", "ws353-ENES", "MC-ES", "ws353-ES"]
    #simTasks = ["MEN", "MC", "ws353", "sl999", "RW", "RG"] #only english
    #simTasks = ["MC-ES", "ws353-ES", "MEN", "MC", "ws353", "sl999", "RW", "RG"] #all
    #metrics = ["cos", "eucl"]
    metrics = ["cos"]

    for simTask in simTasks:
        for metric in metrics:
           evalSimTask(lookupTable, wd, simTask, metric=metric, wiki=wiki, wmt15=wmt15, wmt15wiki=wmt15wiki, lc=lc)
        
        """
        evalSimTask(lookupTable, wd, "MC", metric="eucl", wiki=wiki, wmt15=wmt15, wmt15wiki=wmt15wiki, lc=lc)
        
        evalSimTask(lookupTable, wd, "ws353", metric="cos", wiki=wiki, wmt15=wmt15, wmt15wiki=wmt15wiki, lc=lc)
    evalSimTask(lookupTable, wd, "ws353", metric="eucl", wiki=wiki, wmt15=wmt15, wmt15wiki=wmt15wiki, lc=lc)

    evalSimTask(lookupTable, wd, "sl999", metric="cos", wiki=wiki, wmt15=wmt15, wmt15wiki=wmt15wiki, lc=lc)
        evalSimTask(lookupTable, wd, "sl999", metric="eucl", wiki=wiki, wmt15=wmt15, wmt15wiki=wmt15wiki, lc=lc)
    """
