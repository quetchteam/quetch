
#evaluate the QUETCH embeddings on en-es Cross-lingual Document Classification Tasks
#1) TED
# http://www.clg.ox.ac.uk/tedcorpus
#2) Reuters RCV1/2

#Hermann & Blunsom: 
#use only talk body, lowercase, unique tokens = UNK, suffixed for lang
#compositional vector models (CVM):
#1) ADD: sentence = sum of words
#2) BI: sum over tanh over bigrams sums (SUM(tanh(x_i-1+x_i))
#combine words with CVM to sentences, combine sentences with CVM to documents
#weights randomly intialized with Gaussian u=0, o2 = 0.1
#word embedding dimensionality d=128
#L2 Regularization with lambda=1, step size in {0.01, 0.05}
#100 interations on RCV, 500 on TED 
#Adagrad
#minibatch b 10 or 50
#avg perceptron: 

#alternatives (Klementiev 2012):
#3) sum or avg of vectors 
#4) avg of words weighted by their idf
#word embedding dimensionality d=40
#40 iterations
#learning rate = 0.005
#number of epochs = 10
#filter out words that occur less than 5 times in dataset
#Klementiev: was trained on supervised training data in one language and directly tested on documents in the other.
#with representations of documents, train an averaged perceptron

#here:
#train word2vec and QUETCH on larger dim than before (128 or at least 40)
#read in dataset
#get QUETCH word embeddings
#compute document embeddings
#build sklearn train & dev set
#train sklearn averaged perceptron
#evaluate on dev

#scenarios:
#1) train on en, test on es
#2) train on es, test on en
#3) train on en+es, test on en+es
#4) train on en, test on en
#5) train on es, test on es

import xml.etree.ElementTree as ET
from EvalModel import loadParams
from gensim import models, corpora
import numpy as np
import argparse
from ParameterInspection import dataToDicts15
from nltk.tokenize import word_tokenize
from evalOnWordSimTasks import getQUETCHvectorForWord
from sklearn.linear_model import SGDClassifier
from sklearn.multiclass import OneVsRestClassifier
from sklearn.multiclass import OneVsOneClassifier
from sklearn.svm import LinearSVC
from sklearn import preprocessing
from sklearn import metrics
import codecs
import cPickle
from gensim.corpora import dictionary
from gensim.models import tfidfmodel

keywordFilter = set("technology, culture, science, global issues, design, business, entertainment, arts, politics, education, art, health, creativity, economics, biology".split(", "))


#read the train ted dataset
#15 keywords as topics:
#technology, culture, science, global issues, design, business, entertainment, arts, politics, education, art, health, creativity, economics, biology
def readTrainTED(tedFile, lc):
    #keywordFilter = set("technology, culture, science, global issues, design, business, entertainment, arts, politics, education, art, health, creativity, economics, biology".split(", "))
    
    #no proper xml file
    inTranscript = False
    inTalk = False
    transcripts = list() #body of talk
    talks = dict() #map ids to transcripts
    talkid = 0 #current
    body = list() #list of sentences
    endOfPreviousTalk = False
    skipTalk = False
    countSkipped = 0
    for line in codecs.open(tedFile, "r", "utf-8"):
	if line.startswith("<url"):
	    inTalk = True
	elif "</transcript>" in line: #end of talk
	    additionalText = line.split("</transcript>") #line also contains part of talk body
	    if len(additionalText)>1:
		body.extend(word_tokenize(additionalText[0].lower()) if lc else word_tokenize(additionalText[0].lower()))
	    if not skipTalk:
		talks[talkid] = (keywords, body)
	    #print talkid, keywords, body[0]
	    #reset
	    inTalk = False
	    inTranscript = False
	    talkid = 0
	    body = list()
	    skipTalk = False
	elif "<transcript>" in line:
	    inTranscript = True
	elif "<talkid>" in line:
	    talkid = line.split("<talkid>")[1].split("</talkid>")[0]
	elif "<keywords>" in line:
	    keywords = set(line.split("<keywords>")[1].split("</keywords>")[0].strip().split(",")).intersection(keywordFilter)
	    if len(keywords)==0:
		#print "Not a relevant document, continue"
		skipTalk = True
		countSkipped += 1
	elif inTranscript == True: #talk body
	    body.extend(word_tokenize(line.strip().lower()) if lc else word_tokenize(line.strip()))
	#print body
    print len(talks), "read, ", countSkipped, "skipped" #should be 1153 all in all
    return talks

def matchTalks(tedTrain_en, tedTrain_es): #match ids and keywords
    talkids_en = set(tedTrain_en.keys())
    talkids_es = set(tedTrain_es.keys())
    onlyInEn = talkids_en-talkids_es
    onlyInEs = talkids_es-talkids_en
    #print onlyInEn
    #print onlyInEs
    final_tedTrain_en = dict()
    final_tedTrain_es = dict()
    
    for item in tedTrain_en.items():
	id = item[0]
	talk_en = item[1]
	if id not in onlyInEn: #filter those who are only in en
	    final_tedTrain_en[id] = talk_en
	    
    for item in tedTrain_es.items():
	id = item[0]
	talk_es = item[1]
	#print id
	if id not in onlyInEs:
	    final_tedTrain_es[id] = talk_es
    print "reduced to len:", len(final_tedTrain_en), len(final_tedTrain_es)
    return final_tedTrain_en, final_tedTrain_es
    
def readDevTestTED(tedFile,lc):
    with open(tedFile, "r") as xmlFile:
        tree = ET.parse(xmlFile)
    root = tree.getroot()
    talks = dict()
    #print root.tag, root.attrib
    for child in root:
        #print child.tag, child.attrib
        for childchild in child:
            bodyText = list()
            #print childchild.tag, childchild.attrib #doc {'genre': 'lectures', 'docid': '69'}
            talkid = int(childchild.get("docid"))
            #print talkid
            body = list()
            for childchildchild in childchild.findall("seg"):
                body.extend(word_tokenize(childchildchild.text.strip().lower()) if lc else word_tokenize(childchildchild.text.strip()))
            bodyText.extend(body)
            #print body
            keywords = set(childchild.find("keywords").text.strip().split(",")).intersection(keywordFilter)
            #print keywords
            #print keywords.intersection(keywordFilter)
            if len(keywords)==0:
                continue
            talks[talkid] = (keywords, bodyText)  
    #print bodyText
    return talks      

#get vector representation for talk
#lookup word embeddings in model and combine them to a document vector
def talk2vector(tedTalks, lookupTable, wd, lc=True):
    docs = list()
    text = list()
    #for (talkid, (keywords, body)) in tedTalks.items():
          
    bodies = [body for (talkid, (keywords, body)) in tedTalks.items()] #list of sentences per talk
    print len(bodies), "docs"
    #docs = [[word_tokenize(sentence.lower()) if lc else word_tokenize(sentence) for sentence in doc] for doc in bodies]
    #print docs
    #print bodies[0]
    d = dictionary.Dictionary(documents=bodies)
    print d
    tfidf = tfidfmodel.TfidfModel(dictionary=d)  
    print tfidf  
    #1) (weighted) avg all word vectors
    talkVectors = dict()
    for talk in tedTalks.items():
        (talkid, (keywords, body)) = talk
        #print talkid, keywords
        #body is already tokenized -> list
        #print docs
        #print d
        #print d.token2id
        #print e

        print len(body), "words in talk"
        print len(set(body)), "distinct words"
        unknownWords = set()
        #weights: idf of word
        vectorSum = np.zeros(lookupTable[0].size, dtype=np.float)
        counter = 0
        for w in body:    
            #get embedding
            (vector, errorCode) = getQUETCHvectorForWord(wd, lookupTable, w)
            if errorCode == -1:
                #print "unknown", w
                unknownWords.add(w)
                continue
            #print vector
            #get weight
            #print "word", w, "id", d.token2id[w]
            idf = tfidf.idfs[d.token2id[w]]
            #print "idf", idf
            vectorSum += idf*vector
            counter += idf
        #print vectorSum
        print len(unknownWords), "unknown words"
        vectorAvg = vectorSum/counter
        print talkid, "talk vector:", vectorAvg
        talkVectors[talkid] = (keywords, vectorAvg)
    return talkVectors
    #TODO: more choices

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Evaluate QUETCH trained (WMT15) parameters on cross-lingual document classification task")
    parser.add_argument("ParameterFile", type=str, help="Path to parameters of trained model")
    parser.add_argument("-dict", "--Dictionary", type=str, help="Path to dictionary")
    parser.add_argument("-tc", "--TrueCase", action="store_true", help="Use truecase words; default=False")
    args = parser.parse_args()
    parameterFile = args.ParameterFile
    d = args.Dictionary
    lowercase = not args.TrueCase
    lc = "lc." if lowercase else ""
    wd = dataToDicts15(d=d)
    print len(wd), "words in vocabulary loaded"

    lookupTable = loadParams(parameterFile)[0].get_value()
    print lookupTable.shape, "sized lookup table loaded"

	
    tedTrain_en = readTrainTED("../CLDC/TED/es-en/train.tags.es-en.en", lc)
    tedTrain_es = readTrainTED("../CLDC/TED/es-en/train.tags.es-en.es", lc)
    (tedTrain_en, tedTrain_es) = matchTalks(tedTrain_en, tedTrain_es)

    tedDev_en = readDevTestTED("../CLDC/TED/es-en/IWSLT13.TED.dev2010.es-en.en.xml", lc)
    tedDev_es = readDevTestTED("../CLDC/TED/es-en/IWSLT13.TED.dev2010.es-en.es.xml", lc)
    (tedDev_en, tedDev_es) = matchTalks(tedDev_en, tedDev_es)
    
    tedTest_en = readDevTestTED("../CLDC/TED/es-en/IWSLT13.TED.tst2010.es-en.en.xml", lc)
    tedTest_es = readDevTestTED("../CLDC/TED/es-en/IWSLT13.TED.tst2010.es-en.es.xml", lc)
    (tedTest_en, tedTest_es) = matchTalks(tedTest_en, tedTest_es)
    #print tedDev_en.keys()
    #print tedDev_en[453]
    #selection = [93, 453]
    #smallSet_en = {k:v for (k,v) in tedDev_en.items() if k in selection}
    #smallSet_es = {k:v for (k,v) in tedDev_es.items() if k in selection}
    #tedTrain_en_vector = talk2vector(smallSet_en, lookupTable, wd, lc=lc) 
    #tedDev_es_vector = talk2vector(smallSet_es, lookupTable, wd, lc=lc)
	
    #tedTrain_en_vector = talk2vector(tedTrain_en, lookupTable, wd, lc=lc) 
    tedDev_en_vector = talk2vector(tedDev_en, lookupTable, wd, lc=lc)
   
    #train on en
    #tedTrain_en_vector = talk2vector(tedTrain_en, lookupTable, wd, lc=lc)
    #X = np.array([vector for (keywords, vector) in tedDev_en_vector.values()]) 
    #print X
   # Y = np.array([[keyword2id[keyword] for keyword in keywords] 
    #id2keyword = dict(enumerate(keywordFilter))
    #keyword2id = {v: k for k, v in id2keyword.items()}

    #print keyword2id
    print "Training on en train, testing on en dev"
    """
    xlist = list()
    ylist = list()
    for (keywords, vector) in tedTrain_en_vector.values():
        xlist.append(vector)
        ylist.append([kw for kw in keywords])
        #ylist.append([keyword2id[keyword] for keyword in keywords])
    X = np.array(xlist)
    #Y = np.array(ylist)
    #X = xlist
    Y = ylist
    print "X_train", X
    print "Y_train", Y
    """
    #multi-label, multi-class classification problem
    #lb = preprocessing.LabelBinarizer()
    lb = preprocessing.MultiLabelBinarizer()
   # Y = lb.fit_transform(Y)
    
    #print "Y_train_bin", Y    
    
    X_test = np.array([v for (k,v) in tedDev_en_vector.values()]) #change language here!
    ylist_test = list()
    for (keywords, vector) in tedDev_en_vector.values(): #change languega here
        ylist_test.append([kw for kw in keywords])
    Y_test_word = ylist_test
    print "Y_test", Y_test_word

    #Y_test = np.array([kw for kw in [k for (k,v) in tedDev_es_vector.values()]])
    Y_test = lb.fit_transform(Y_test_word)
    print "X_test", X_test
    print "Y_test_bin", Y_test

    #clf = OneVsRestClassifier(LinearSVC())
    #print "Training OneVsRest..."
    
    #clf = OneVsOneClassifier(LinearSVC())   
    #print "Training OneVsOne..."
    
    #clf = OneVsOneClassifier(SGDClassifier(loss="perceptron", average=True))
    #clf = SGDClassifier(average=True) #averaged perceptron

    #clf.fit(X, Y)
    
    modelFile = "1vs1_SGDPerceptron_d50_idfweighted_avg_en.pkl"
    
   # print "Storing the classifier in", modelFile
    #with open(modelFile, "wb") as fid:
   #     cPickle.dump(clf, fid)
    clf = cPickle.load(modelFile)
    print "Loading model from file", modelFile
    print "Predicting..."
    predicted = clf.predict(X_test)
    print "Raw prediction", predicted
    all_labels = lb.inverse_transform(predicted)
    #all_labels = predicted
    print "Predicted:", all_labels
    for item, labels, trueLabels in zip(X_test, all_labels, Y_test_word):
        print "%s => %s, true: %s" % (item, ', '.join(labels), ', '.join(trueLabels))

    #print "F1", metrics.f1_score(Y_test, all_labels) 
    #print "Acc", metrics.accuracy_score(Y_test, all_labels)
    #print "Precision", metrics.precision_score(Y_test, all_labels)
    #print "Recall", metrics.recall_score(Y_test, all_labels)
    print metrics.classification_report(Y_test, all_labels)
    
