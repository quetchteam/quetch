#-*- coding: utf-8 -*-

import numpy as np
from sklearn.decomposition import TruncatedSVD
from sklearn.manifold import TSNE
from sklearn.decomposition import PCA
from ParameterInspection import dataToDicts15
from gensim import models, corpora
from EvalModel import loadParams
import matplotlib
matplotlib.use('GTK')
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d import proj3d
from mpl_toolkits.axes_grid.anchored_artists import AnchoredText

if __name__=="__main__":

    words = []
    
    #word similarity/relatedness:
    
    #hyponym, hyperonym: banana, fruit, apple;
    #morphologic: love, loves; hate, hates; i, you
    #syntactic/functional: and, but; ?, .
    #antonyms: love, hate; 
    #synonyms (similar): buy, purchase; big, large; person, human, people; cat dog tortoise
    #contextual: love you, i was, it is
    #+spanish translations
    
    words = u"banana plátano fruit fruta apple manzana internet drama love amor loves amar hate odio hates odiar i yo you tú and y but pero ? ¿ buy comprar purchase compra big grande large gran person persona human humano people gente cat gato dog perro tortoise tortuga was estaba it está is es".split()
    #words = ["fourth", "50s", "80s","eighties", "cat", "dog","banana", "quiero", "odiar","time", "tiempos", "y", "and", "...", "nokia", "music", "friday", "monday", "lunes", "viernes", "semana", "fine","?", "internet", "fruit", "fruits", "tiempos", "year", u"año","eat", "comer", "people", "personas", "persons", "apple", "manzana", "facebook", "google", "love", "hate", "amor", "odiar", "loves", "odio"]

    color = ['0.0', '1', '0.0', '1', '0.0', '1', '0.5', '0.5','0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1', '0.0', '1']

    modelDict = {"submitted":"../parameters/2015-06-06--11:50:46.512444.15.params", "late": "../parameters/2015-06-26--13:48:39.961161.345.params", "vanilla":"../parameters/2015-06-06--11:42:27.119929.323.params", "word2vec":"../parameters/2015-06-06--11:50:46.512444.0.params"}

    for model, param in modelDict.items():
        print "Loading sims for model", model
        #for QUETCH model   
        #parameterFile = "../parameters/2015-06-06--11:50:46.512444.55.params"
        #parameterFile = "../parameters/2015-06-06--11:49:22.916009.200.params"
        #../dicts/2015-07-28--08:48:39.880538.dict
        #parameterFile = "../parameters/2015-07-28--08:48:43.132513.5.params" #128dims
        parameterFile = param
        lt = loadParams(parameterFile)[0].get_value()
        wd = dataToDicts15()

        #TODO do svd on FULL data, not only selection!
        wordlist = list()
        for w in words:
            iw = wd.token2id[w]
            #print w, iw, lt[iw]
            wordlist.append(lt[iw])
        wordmatrix =  np.array(wordlist)
        
        
        transformed_svd = TruncatedSVD(n_components=2).fit_transform(wordmatrix)
        transformed_tsne = TSNE(n_components=2).fit_transform(wordmatrix)
        transformed_pca = PCA(n_components=2).fit_transform(wordmatrix)

        xs_svd = [t[0] for t in transformed_svd]
        ys_svd = [t[1] for t in transformed_svd]

        xs_tsne = [t[0] for t in transformed_tsne]
        ys_tsne = [t[1] for t in transformed_tsne]

        xs_pca = [t[0] for t in transformed_pca]
        ys_pca = [t[1] for t in transformed_pca]



        plt.figure()
        #plt.subplot(211)
        plt.scatter(xs_svd,ys_svd, c=color)
        for l,x,y in zip(words, xs_svd, ys_svd):
            plt.annotate(l, xy=(x,y))
        #plt.title("Word representation projected onto 2-dim space with SVD")
        plt.savefig("QUETCHwords_"+model+"_svd.eps", format='eps', dpi=1000)
        
        """
        plt.figure()
        #plt.subplot(211)
        plt.scatter(xs_tsne,ys_tsne)
        for l,x,y in zip(words, xs_tsne, ys_tsne):
            plt.annotate(l, xy=(x,y))
        plt.title("Word representation projected into 2-dim space with t-SNE")
        plt.savefig("QUETCHwords_"+model+"_tsne.png")
        """

        """
        plt.figure()
        #plt.subplot(211)
        plt.scatter(xs_pca,ys_pca)
        for l,x,y in zip(words, xs_pca, ys_pca):
            plt.annotate(l, xy=(x,y))
        plt.title("Word representation projected into 2-dim space with PCA")
        plt.savefig("QUETCHwords_"+model+"_pca.png")
        """

        """
        #now 3d!
        
        transformed_svd = TruncatedSVD(n_components=3).fit_transform(wordmatrix)
        transformed_tsne = TSNE(n_components=3).fit_transform(wordmatrix)
        transformed_pca = PCA(n_components=3).fit_transform(wordmatrix)

        xs_svd = [t[0] for t in transformed_svd]
        ys_svd = [t[1] for t in transformed_svd]
        zs_svd = [t[2] for t in transformed_svd]

        xs_tsne = [t[0] for t in transformed_tsne]
        ys_tsne = [t[1] for t in transformed_tsne]
        zs_tsne = [t[2] for t in transformed_tsne]


        xs_pca = [t[0] for t in transformed_pca]
        ys_pca = [t[1] for t in transformed_pca]
        zs_pca = [t[2] for t in transformed_pca]

        global labels_and_points
        labels_and_points = []

        fig = plt.figure()
        #plt.subplot(211)
        ax = fig.add_subplot(111,projection='3d')
        ax.scatter(xs_svd,ys_svd,zs_svd)
        for l,x,y,z in zip(words, xs_svd, ys_svd, zs_svd):
            x2, y2, _ = proj3d.proj_transform(x,y,z, ax.get_proj())
            label = plt.annotate(
                l, xy = (x2, y2), xytext = (-5,5), textcoords='offset points', ha='left', va='bottom',
                arrowprops = dict(arrowstyle = '-', connectionstyle = 'arc3,rad=0.3'))
            labels_and_points.append((label, x, y, z))
        plt.title("Word representation projected onto 3-dim space with SVD")
        
        def update_position(e):
            for label, x, y, z in labels_and_points:
                x2, y2, _ = proj3d.proj_transform(x, y, z, ax.get_proj())
                label.xy = x2,y2
                label.update_positions(fig.canvas.renderer)
            fig.canvas.draw()

        fig.canvas.mpl_connect('motion_notify_event', update_position)

        #plt.show()
        plt.savefig("QUETCHwords_"+model+"svd_3d.png")

        """
        
        
    """
    #for QUETCH model without pre-training       
        parameterFile = "../parameters/2015-06-06--11:42:27.119929.305.params"
        lt = loadParams(parameterFile)[0].get_value()
        wd = dataToDicts15()

        wordlist = list()
        for w in words:
                iw = wd.token2id[unicode(w)]
                print w, iw, lt[iw]
                wordlist.append(lt[iw])
        wordmatrix =  np.array(wordlist)
        
        transformed = svd.fit_transform(wordmatrix)
        
        xs = [t[0] for t in transformed]
        ys = [t[1] for t in transformed]

        plt.figure()
        #plt.subplot(211)
        plt.scatter(xs,ys)
        for l,x,y in zip(words, xs, ys):
                plt.annotate(l, xy=(x,y))
        plt.title("plain QUETCH word representation projected into 2-dim space")
        plt.savefig("QUETCHwords_plain.png")


    #for word2vec wiki model
    modelName = "features/wiki.en-es.word2vec.w5.d10.lc.model"
        model =  models.Word2Vec.load(modelName)
    wordlist = list()
    for w in words:
        wordlist.append(model[w])
    wordmatrix2 = np.array(wordlist)
    
    transformed2 = svd.fit_transform(wordmatrix2)
    
    wxs = [t[0] for t in transformed2]
    wys = [t[1] for t in transformed2]

    plt.figure()
    #plt.subplot(212)
    plt.scatter(wxs, wys)
    for l,x,y in zip(words, wxs,wys):
        plt.annotate(l, xy=(x,y))
    plt.title("word2vec word representations projected into 2-dim space")
    #plt.show()

    plt.savefig("word2vecwords.png")
    """
