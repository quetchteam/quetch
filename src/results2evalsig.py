import sys

#read in results and prepare for evaluation script that checks significance etc.

def formatQuetch(resultsFile, outputFile):
	r = open(resultsFile, "r")
	o = open(outputFile, "w")

	currentSent = "0.0"
	outputLine = list()
	for line in r:
		#print currentSent
		#format: 10.0    0       El      OK      OK      OK
		(segmentNumbPoint, wordIndex, word, multi, l1, binary) = line.strip().split()
		if segmentNumbPoint == currentSent:
			#append to current line
			#print "still in sentence"
			#print line
			#print binary
			outputLine.append(binary)
			#print outputLine

		else:
			#print "new sentence", line
			#print "after sentence", currentSent, "now sentence", segmentNumbPoint
			currentSent = segmentNumbPoint
			o.write(" ".join(outputLine)+"\n")
			outputLine = list()
			outputLine.append(binary) #start of the next sentence
	#print last sentence
	o.write(" ".join(outputLine)+"\n")
	o.close()
	r.close()

def vwout2label(vwout):
	#vw output is float, <= 0 -> BAD, > 0 -> OK
	vwoutfl = float(vwout)
	if vwoutfl <= 0:
		return "BAD"
	else:
		return "OK"

def formatVW(resultsFile, outputFile):
	r = open(resultsFile, "r")
        o = open(outputFile, "w")
	#TODO

if __name__ == "__main__":
	
	resultsFile = sys.argv[1]
        outputFile = sys.argv[2]

	if resultsFile.endswith(".results"): #QUETCH 
                formatQuetch(resultsFile, outputFile)
        elif resultsFile.endswith(".out"): #VW
                formatVW(resultsFile, outputFile)
        else:
                print "Couldn't decide whether QUETCH or VW output given"

