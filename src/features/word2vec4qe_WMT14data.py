import sys
import gensim.models
import numpy as np
import nltk
import tarfile
import codecs

""" 
Training a Word2Vec model on English and Spanish WMT14 data.
The trained vectors of fixed size can be used to initialize the QUETCH Lookup-Table.
Parameters: 1)d_wrd 2)model output file
""" 


if __name__=="__main__":
	#open input files (en + es)
	en_files = ["../../WMT14-data/task2_en-es_test/EN_ES.source.sentences.test", "../../WMT14-data/task2_en-es_training/EN_ES.source.sentences.train" ]
	es_files = ["../../WMT14-data/task2_en-es_test/EN_ES.tgt_ann.sentences.test", "../../WMT14-data/task2_en-es_training/EN_ES.tgt_ann.sentences.train"]
	

	#wiki corpus format: <doc id="ID" ... > TEXT\nENDOFARTICLE.</doc>
	sents = [["UNKNOWN"]]
	for f in en_files:
		print "Reading EN documents from file", f
		infile = codecs.open(f, "r", "utf8")
		indoc = False
		for line in infile:
			if len(line)<2:
				continue
			tokenized = nltk.word_tokenize(line)
			sents.append(tokenized)
	print "Read all english documents."

	for f in es_files:
		print "Reading ES documents from file", f
		infile = codecs.open(f, "r", "utf8")
		indoc = False
		for line in infile:
			if len(line)<2:
				continue
			tokenized = nltk.word_tokenize(line)
			sents.append(tokenized)
	print "Read all spanish documents."

	print "Read %d sentences" % len(sents)

	#build gensim model
	windowsize = int(sys.argv[1])
	print "Building the word2vec model with windowsize=%d" % windowsize
	
	model = gensim.models.word2vec.Word2Vec(sents, size=50, window=windowsize, min_count=0, workers=8)

	#persist model
	modelfile = sys.argv[2]
	print "Persisting the model in %s" % modelfile
	model.save(modelfile)
	print "Done."

	print model["enjoy"]
	print model["Europa"]
	print model.most_similar(positive=["Europa"])
	print model.similarity("China", "Indonesia")

