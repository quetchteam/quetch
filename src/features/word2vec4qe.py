#-*- coding: utf-8 -*-

import sys
import gensim.models
import numpy as np
import nltk
import tarfile
import codecs

""" 
Training a Word2Vec model on English and Spanish Wikipedia corpus.
The trained vectors of fixed size can be used to initialize the QUETCH Lookup-Table.
Parameters: 1)window 2)d_wrd 2)model output file
""" 


if __name__=="__main__":
	#open input files (en + es)
	en_archive = "../../Wikicorpus-data/raw.en.tgz"
	es_archive = "../../Wikicorpus-data/raw.es.tgz"
	if not tarfile.is_tarfile(en_archive):
		print "Input is not a tarfile."
		sys.exit(-1)
	if not tarfile.is_tarfile(es_archive):
		print "Input is not a tarfile."
		sys.exit(-1)

	#several files stored in tgz
	en_tar = tarfile.open(en_archive, "r:gz", encoding="latin-1", errors="replace")
	es_tar = tarfile.open(es_archive, "r:gz", encoding="latin-1", errors="replace")
	

	#wiki corpus format: <doc id="ID" ... > TEXT\nENDOFARTICLE.</doc>
	#sents = [["UNKNOWN","PADDING"]]
	sents = []

	
	#i=0
	for tarf in en_tar:
		#i+=1
		print "Reading EN documents from file", tarf.name
		infile = en_tar.extractfile(tarf)
		indoc = False
		for line in infile:
			if len(line)<2:
				continue
			if "<doc" in line:
				#print "Current EN document ID:", line.split()[1].split("=")[1]
				continue
			if "</doc" in line or "ENDOFARTICLE" in line:
				continue
			tokenized = nltk.word_tokenize(line.decode("latin-1").lower())
			#print tokenized
			sents.append(tokenized)
		#if i>0:
		#	break
	print "Read all english documents."
	

	i = 0
	for tarf in es_tar:
		i+=1
		print "Reading ES documents from file", tarf.name
		infile = es_tar.extractfile(tarf)
		indoc = False
		for line in infile:
			if len(line)<2:
				continue
			if "<doc" in line:
				#print "Current ES document ID:", line.split()[1].split("=")[1]
				continue
			if "</doc" in line or "ENDOFARTICLE" in line:
				continue
#			print line
			tokenized = nltk.word_tokenize(line.decode("latin-1").lower())
#			print tokenized
			sents.append(tokenized)
		#if i>0:
		#	break
	print "Read all spanish documents."

	print "Read %d sentences" % len(sents)

	#build gensim model
	windowsize = int(sys.argv[1])
	dimsize = int(sys.argv[2])
	print "Building the word2vec model with windowsize=%d and vector dimensionality=%d" % (windowsize, dimsize)
	
	model = gensim.models.word2vec.Word2Vec(sents, size=dimsize, window=windowsize, min_count=0, workers=20)

	#persist model
	modelfile = sys.argv[3]
	print "Persisting the model in %s" % modelfile
	model.save(modelfile)
	print "Done."

	#test model
	print "Testing the model with most similar words for 'el'",model.most_similar(positive=["el"])
        print "Testing the model with most similar words for 'the'",model.most_similar(positive=["the"])



