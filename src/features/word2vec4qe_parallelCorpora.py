import sys
import gensim.models
import numpy as np
import nltk
import tarfile
import codecs

""" 
Training a Word2Vec model on parallel English and Spanish corpora.
The trained vectors of fixed size can be used to initialize the QUETCH Lookup-Table.
Parameters: 1)context window size 2)d_wrd 3)model output file
""" 


if __name__=="__main__":
	#open input files (en + es)

	commoncrawl_files = ["../../parallelCorpora-data/commoncrawl.es-en.en.norm.tok", "../../parallelCorpora-data/commoncrawl.es-en.es.norm.tok"]
	europarl_files = ["../../parallelCorpora-data/europarl-v7.es-en.es.norm.tok", "../../parallelCorpora-data/europarl-v7.es-en.en.norm.tok"]
        newscorpus_files = ["../../parallelCorpora-data/news-commentary-v8.es-en.en.norm.tok", "../../parallelCorpora-data/news-commentary-v8.es-en.es.norm.tok"]


	#format: one sentence per line, tokenized and tab-separated
	sents = []
	for f in commoncrawl_files:
		print "Reading commoncrawl documents from file", f
		infile = codecs.open(f, "r", "utf8")
		indoc = False
		for line in infile:
			tokenized = line.split() #already pre-processed (tokenized)
			sents.append(tokenized)
			#print sents
	print "Read all commoncrawl documents."

	for f in europarl_files:
		print "Reading europarl documents from file", f
		infile = codecs.open(f, "r", "utf8")
		indoc = False
		for line in infile:
			tokenized = line.split()
			sents.append(tokenized)
	print "Read all europarl documents."


        for f in newscorpus_files:
                print "Reading newscorpus documents from file", f
                infile = codecs.open(f, "r", "utf8")
                indoc = False
                for line in infile:
                        tokenized = line.split()
                        sents.append(tokenized)
        print "Read all newscorpus documents."


	print "Read %d sentences" % len(sents)

	#build gensim model
	windowsize = int(sys.argv[1])
	dimsize = int(sys.argv[2])
	print "Building the word2vec model with windowsize=%d and %d dimensions" % (windowsize, dimsize)
	
	model = gensim.models.word2vec.Word2Vec(sents, size=dimsize, window=windowsize, min_count=0, workers=8)

	#persist model
	modelfile = sys.argv[3]
	print "Persisting the model in %s" % modelfile
	model.save(modelfile)
	print "Done."

	print model["enjoy"]
	print model["Europa"]
	print model.most_similar(positive=["Europa"])
	print model.similarity("China", "Indonesia")

