import sys

"""Extract raw sentences from WMT14 data to annotate with other tools. Output format: one sentence per line"""

if __name__=="__main__":
	filename = sys.argv[1]
	outname = sys.argv[2]
	fin = open(filename, "r")
	fout = open(outname, "w")
	if "source" in filename: #source data
		for line in fin: #format: 1.1 sentence (not tokenized)
			splitted = line.split("\t")
			sentence = splitted[1].strip()
			fout.write(sentence+"\n")
	elif "tgt" in filename or "target" in filename: #target data
		currentsent =  1.1
		sent = list()
		for line in fin: #format: 1.1	0	word label label label
			splitted = line.split("\t")
			sentindex = splitted[0]
			wordindex = splitted[1]
			word = splitted[2]
			if sentindex == currentsent: #continue reading sentence
				sent.append(word)
			else:
				fout.write(" ".join(sent)+"\n")
				sent = list() #empty
				sent.append(word)
			currentsent = sentindex
			
	fin.close()
	fout.close()

		
