
#evaluate the QUETCH embeddings on en-es Cross-lingual Document Classification Tasks
#1) TED
# http://www.clg.ox.ac.uk/tedcorpus
#2) Reuters RCV1/2

#Hermann & Blunsom: 
#use only talk body, lowercase, unique tokens = UNK, suffixed for lang
#compositional vector models (CVM):
#1) ADD: sentence = sum of words
#2) BI: sum over tanh over bigrams sums (SUM(tanh(x_i-1+x_i))
#combine words with CVM to sentences, combine sentences with CVM to documents
#weights randomly intialized with Gaussian u=0, o2 = 0.1
#word embedding dimensionality d=128
#L2 Regularization with lambda=1, step size in {0.01, 0.05}
#100 interations on RCV, 500 on TED 
#Adagrad
#minibatch b 10 or 50
#avg perceptron: 

#alternatives (Klementiev 2012):
#3) sum or avg of vectors 
#4) avg of words weighted by their idf
#word embedding dimensionality d=40
#40 iterations
#learning rate = 0.005
#number of epochs = 10
#filter out words that occur less than 5 times in dataset
#Klementiev: was trained on supervised training data in one language and directly tested on documents in the other.
#with representations of documents, train an averaged perceptron

#here:
#train word2vec and QUETCH on larger dim than before (128 or at least 40)
#read in dataset
#get QUETCH word embeddings
#compute document embeddings
#build sklearn train & dev set
#train sklearn averaged perceptron
#evaluate on dev

#scenarios:
#1) train on en, test on es
#2) train on es, test on en
#3) train on en+es, test on en+es
#4) train on en, test on en
#5) train on es, test on es

from EvalModel import loadParams
from gensim import models, corpora
import numpy as np
import argparse
from ParameterInspection import dataToDicts15
from nltk.tokenize import word_tokenize
from sklearn.linear_model import SGDClassifier
from sklearn import metrics
from sklearn import grid_search
import codecs
import cPickle
from gensim.corpora import dictionary
from gensim.models import tfidfmodel
import os
import sys

keywordFilter = set("technology, culture, science, global, design, business, entertainment, arts, politics, education, art, health, creativity, economics, biology".split(", "))
#keywordFilter= set(["culture", "science"])

"""
Read text from talk file

Format:
one sentence per line, lowercased, suffixes for language
e.g. believe_en me_en ,_en those_en little_en classrooms_en are_en really_en noisy_en ._en
"""
def readText(fileName):
    text = list()
    with open(fileName, "r") as f:
        for line in f:
            words = [word.replace("_en","").replace("_es","") for word in line.split() if "UNK" not in word] #strip suffixes, remove UNK, TODO more languages
            #print words
            text.extend(words)
    return text

"""
Get the word embedding from the lookuptable
"""
def getQUETCHvectorForWord(wd, lt, word):
    errorCode = 0
    try:
        i = wd.token2id[word]
    except KeyError:
        i = wd.token2id["UNKNOWN"]
        errorCode = -1
    vector = lt[i]
    return vector, errorCode

"""
Transform a text into its vector representation

Several approaches to compose vectors of one document:
1) take average
2) take sum
3) take idf-weighted average (Huang et al., 2012)
4) ...

"""
def text2vector(text, lookupTable, wordDictionary, tfidf, d, lc=True):
    #print len(text), "words in talk"
    #print len(set(text)), "distinct words"
    unknownWords = set()
    #weights: idf of word
    vectorSum = np.zeros(lookupTable[0].size, dtype=np.float)
    counter = 0.0
    for w in text:    
        #get embedding
        (vector, errorCode) = getQUETCHvectorForWord(wordDictionary, lookupTable, w)
        if errorCode == -1:
            #print "unknown", w
            unknownWords.add(w)
            continue
        #print vector
        #get weight
        #print "word", w, "id", d.token2id[w]
        if tfidf is not None and d is not None: #tfidf weighting
            idf = tfidf.idfs[d.token2id[w]]
            #print "idf", idf, "counter", counter
            vectorSum += idf*vector
            counter += idf
        else: #averaging
            vectorSum += vector
            counter += 1
            #print vectorSum
    #print " %d unknown words" % len(unknownWords)
    vectorAvg = vectorSum/counter #averaging
    #vectorAvg = vectorSum #sum
    #print vectorAvg
    return vectorAvg

"""
Load TED CLDL data for a given category

Directory structure:
languagePair
-test
--category
---negative
----id.ted
---positive
----id.ted
-train
--category
---negative
----id.ted
---positive
----id.ted
"""
def loadData(dataDir, keyword, lt, wd, lc=True):
    
    #for train and test:
    #open files
    #read texts
    #transform to vectors
    #label with category (-1, 1)
    
    #output: X, Y, X_test, Y_test
    
    partitions = ["train", "test"]
    categories = ["positive", "negative"]
    category2label = {"positive":1, "negative":-1}
    X = list()
    X_test = list()
    Y = list()
    Y_test = list()
    texts = list()
    tfidf = None
    d = None

    print "Creating tf-idf model"
    for partition in partitions:
        for category in categories:
            curDir = dataDir+"/"+partition+"/"+keyword+"/"+category
            #print "Loading %s %s data" % (partition, category)
            files = os.listdir(curDir)
            print "%d files" % (len(files))
        #first read all of them to get idf weights (TODO: kind of internal idfs, since not whole corpus is considered, but only topic)
            #print "Creating Tfidf model"
            # stop = 1
            # counter = 0
            for f in files:
              #if counter > stop:
             #    break
                #counter += 1
                text = readText(curDir+"/"+f)
                #print text 
                texts.append(text)
                #build tfidf model
    d = dictionary.Dictionary(documents=texts)
    tfidf = tfidfmodel.TfidfModel(dictionary=d)  
    
    for partition in partitions:
        for category in categories:
            print "Loading %s %s data" % (partition, category)
            curDir = dataDir+"/"+partition+"/"+keyword+"/"+category
            files = os.listdir(curDir)
            print "%d files" % (len(files))

            #read again, now transform texts to vectors
            print "Turning document into vector"
            # stop = 1
            #counter = 0
            for f in files:
            #if counter > stop:
            #    break
            #counter += 1
                text = readText(curDir+"/"+f)
                vector = text2vector(text, lt, wd, tfidf, d, lc=lc)
        
                if partition=="train":
                    X.append(vector)
                    Y.append(category2label[category])
                elif partition=="test":
                    X_test.append(vector)
                    Y_test.append(category2label[category])
            
    print "Should match: %d Xs, %d Ys, %d X_tests, %d Y_tests" % (len(X), len(Y), len(X_test), len(Y_test)) 
    return np.array(X), np.array(Y), np.array(X_test), np.array(Y_test)

if __name__ == "__main__":
    
    #d40 lc best F1:
    #../dicts/2015-07-28--08:48:26.466270.dict
    #../parameters/2015-07-28--08:48:31.189894.7.params
    
    #d128 lc best F1:
    #../dicts/2015-07-28--08:48:39.880538.dict
    #../parameters/2015-07-28--08:48:43.132513.5.params
    
    parser = argparse.ArgumentParser(description="Evaluate QUETCH trained (WMT15) parameters on cross-lingual document classification task")
    parser.add_argument("languagePair", type=str, help="language pair")
    parser.add_argument("ParameterFile", type=str, help="Path to parameters of trained model")
    parser.add_argument("-dict", "--Dictionary", type=str, help="Path to dictionary")
    parser.add_argument("-tc", "--TrueCase", action="store_true", help="Use truecase words; default=False")
    args = parser.parse_args()
    parameterFile = args.ParameterFile
    d = args.Dictionary
    lowercase = not args.TrueCase
    lc = "lc." if lowercase else ""
    languagePair = args.languagePair

    validLanguages = ["en-es", "es-es", "en-en", "es-en"]
    if languagePair not in validLanguages:
        print "Please enter a valid language, one of: ", validLanguages
        sys.exit(-1)


    wd = dataToDicts15(d=d)
    print len(wd), "words in vocabulary loaded from", d

    lookupTable = loadParams(parameterFile)[0].get_value()
    print lookupTable.shape, "sized lookup table loaded from", parameterFile

    #for each keyword, train a separate classifier
    rs = list()
    ps = list()
    f1s = list()
    for keyword in keywordFilter:
        print "="*10, keyword.upper(),"="*10
    
        dataDir = "../CLDC/TED/ted-cldc/"+languagePair
        print "Loading data from", dataDir
    
        #load data from dir
        X, Y, X_test, Y_test = loadData(dataDir, keyword, lookupTable, wd, lc=lc)
    
        #perform grid search
        parameters = {"class_weight": ["balanced"], "n_iter": [10,50,100,150,200,300], "average":[True, False]}
           #"loss": ["perceptron","hinge","log"], "penalty": ["l2", "l1", "elasticnet"], 
           #"learning_rate":["constant", "optimal", "invscaling"],
           #"average":[True, False], "alpha":[0.0001], "l1_ratio":[0.15], "fit_intercept":[True],
           #"shuffle": [True], "random_state":[None], "epsilon":[0.1], "eta0":[0.00001],
           #"power_t":[0.5]}
    
    #small test
    #parameters = {"n_iter":(1,2)}
    
        print "\nTraining", languagePair

        #traditional
        clf = SGDClassifier(class_weight="balanced", n_iter=10)

        #Grid search
        #sgd = SGDClassifier() #TODO for RCV use averaged perceptron (loss="perceptron", average=True) in multi-task fashion, for ted use ??
    #n_iter=100, class_weight="balanced", loss="perceptron"
    
        #TODO: use the same configurations for all keywords
        #TODO: use same classifier to train on corresponding word2vecs and report differences
    
        #clf = grid_search.GridSearchCV(sgd, param_grid=parameters, scoring="f1_macro", iid=False, cv=2)
    
    
        clf.fit(X, Y)

        #print "Best scores on left out data", clf.best_score_, "best params: ", clf.best_params_
        #bestclf = clf.best_estimator_
        bestclf = clf

        #classifierType = bestclf.__class__.__name__
        #modelFile = "../clf/"+classifierType+"_"+keyword+"_"+languagePair+"_"+str(clf.best_params_).strip("{}").replace(":","=").replace(" ","").replace(",","_").replace("'","")+".clf"
    #print "\nDumping classifier to file", modelFile
    #with open(modelFile, "wb") as fid:
    #    cPickle.dump(bestclf, fid)

        print "\nPredicting"
        predicted = bestclf.predict(X_test)
    
        #for item, labels, trueLabels in zip(X_test, predicted, Y_test):
        #    print "%s => %s, true: %s" % (item, labels, trueLabels)

        print "\nEvaluating"
        print "F1", metrics.f1_score(Y_test, predicted) 
        f1s.append(metrics.f1_score(Y_test, predicted))
        print "Acc", metrics.accuracy_score(Y_test, predicted)
        print "Precision", metrics.precision_score(Y_test, predicted)
        ps.append(metrics.precision_score(Y_test, predicted))
        print "Recall", metrics.recall_score(Y_test, predicted)
        rs.append(metrics.recall_score(Y_test, predicted))
        print metrics.classification_report(Y_test, predicted)
        print metrics.confusion_matrix(Y_test, predicted)
    
        print "="*30

    print "Final evaluation"
    print "="*30
    print "F1s", f1s 
    print "Recalls", rs
    print "Precisions", ps
    print "Macro average R", np.mean(rs)
    print "Macro average P", np.mean(ps)
    print "Macro average F1:", (2*(np.mean(rs)*np.mean(ps))/(np.mean(rs)+np.mean(ps)))
