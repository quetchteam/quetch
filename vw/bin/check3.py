#!/usr/bin/env python
# -*- coding: utf-8 -*-


import sys, string


fn1 = 0
fn2 = 0

tn1 = 0
tn2 = 0

fp1 = 0
fp2 = 0

tp1 = 0
tp2 = 0

total = 0

for line in sys.stdin:
    #print line
    values = line.split('\t')

    label = float(values[0])
    pred1 = float(values[1])
    pred2 = float(values[2])

    if(label<0):
        if pred1>0:
            fp1 += 1
        else:
            tn1 += 1
        if pred2>0:
            fp2 += 1
        else:
            tn2 += 1

    else: # label > 0
        if pred1<0:
            fn1 += 1
        else:
            tp1 += 1
        if pred2<0:
            fn2 += 1
        else:
            tp2 += 1

    total += 1


acc1 = float(total-fp1-fn1)/total
acc2 = float(total-fp2-fn2)/total


ok_rec1 = tp1/float(tp1 + fn1)
ok_rec2 = tp2/float(tp2 + fn2)

ok_pre1 = tp1/float(tp1+fp1)
ok_pre2 = tp2/float(tp2+fp2)

ok_f11 = 2*(ok_rec1*ok_pre1)/(ok_rec1+ok_pre1)
ok_f12 = 2*(ok_rec2*ok_pre2)/(ok_rec2+ok_pre2)


bad_rec1 = tn1/float(tn1 + fp1)
bad_rec2 = tn2/float(tn2 + fp2)

bad_pre1 = tn1/float(tn1+fn1)
bad_pre2 = tn2/float(tn2+fn2)

bad_f11 = 2*(bad_rec1*bad_pre1)/(bad_rec1+bad_pre1)
bad_f12 = 2*(bad_rec2*bad_pre2)/(bad_rec2+bad_pre2)


print "Total: %d"%total
print "-------------------------------------"
print "System A: fp=%d, fn=%d, acc=%f"%(fp1,fn1,acc1)
print "-------------------------------------"
print "\tREFERENCE"
print "PREDICT\tok\tbad"
print "ok\t%d\t%d"%(tp1,fp1)
print "bad\t%d\t%d"%(fn1,tn1)
print "-------------------------------------"
print "\tOK : recall=%f, precision=%f, f1=%f"%(ok_rec1,ok_pre1, ok_f11)
print "\tBAD: recall=%f, precision=%f, f1=%f"%(bad_rec1,bad_pre1, bad_f11)

print "====================================="
print "-------------------------------------"
print "System B: fp=%d, fn=%d, acc=%f"%(fp2,fn2,acc2)
print "-------------------------------------"
print "\tREFERENCE"
print "PREDICT\tok\tbad"
print "ok\t%d\t%d"%(tp2,fp2)
print "bad\t%d\t%d"%(fn2,tn2)
print "-------------------------------------"
print "\tOK : recall=%f, precision=%f, f1=%f"%(ok_rec2,ok_pre2, ok_f12)
print "\tBAD: recall=%f, precision=%f, f1=%f"%(bad_rec2,bad_pre2, bad_f12)

