#!/usr/bin/env python
# -*- coding: utf-8 -*-


import sys, string



# 0       1       2       3       4       5       6       7               8       9                                                                                       20      21      22      23      24
#
# 5.0     5.0     1.0     ¿       _START_ Quiénes WHO     _START_|_START_ ARE|YOU 0       0       0       0       0       0       0.1     0.1     0.2     0       0       0       FS      WP      1       0       OK

def valsplit(s):
    return string.replace(s,'|',' ')


label = {'OK':1, 'BAD':-1}

numerical = [0,1,2,9,10,11,12,13,14,15,16,17,18,19,20,23,24]

for line in sys.stdin:
    #print line
    values = line.split('\t')

    print label[values[25].strip()],
    print "1.0",

    print "|Numerical",
    for i in numerical:
        print "%d:%s "%(i,values[i]),
    
    print "|Target",values[3].lower(),
    print "|Context",values[4].lower(),values[5].lower(),

    print "|Source",values[6].lower(),
    print "|LeftContext",valsplit(values[7]).lower(),
    print "|RightContext",valsplit(values[8]).lower(),

    print "|POS",values[21],
    print "|AlignedPOS",valsplit(values[22])
    
