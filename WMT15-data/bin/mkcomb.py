#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

basename = sys.argv[1]

targetname = basename+".target"
tagname = basename+".tags"

with open(targetname, "r") as tf:

	targets = tf.readlines()
	tf.close()


with open(tagname, "r") as tf:

	tags = tf.readlines()
	tf.close()


if len(tags) != len(targets):
	sys.stderr.write("ERROR: sentence number mismatch (%d != %d)\n"%(len(tags),len(targets)))
	sys.exit(-1)

ii = len(tags)

for i in xrange(ii):

	tag = tags[i].strip().split()
	target = targets[i].strip().split()

	if len(tag) != len(target):
		sys.stderr.write("ERROR: sentence length mismatch (%d != %d)\n"%(len(tags),len(targets)))
		sys.exit(-1)

	jj = len(tag)

	for j in xrange(jj):
		print "%d\t%d\t%s\t%s"%(i,j,target[j],tag[j])


