#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

basename = sys.argv[1]

featname = basename+".features"

with open(featname, "r") as tf:

	features = tf.readlines()
	tf.close()

ii = len(features)

for i in xrange(ii):

	feature = features[i].strip().split()

	if len(feature)>0:

		sys.stdout.write("%s\t%s\t%s"%(feature[0],feature[1],feature[2]))

		for j in xrange(3,9):
			sys.stdout.write("\t%s"%feature[j])

		for j in xrange(9,26):
			sys.stdout.write("\t%s"%feature[j])

		sys.stdout.write("\n")

