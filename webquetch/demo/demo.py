#-*- coding: UTF-8 -*-

import cPickle
from NN import NN
import theano.tensor as T
from Task import WMT14QETask2, WMT14QETask1_1, WMT15QETask2
import theano
import datetime
#from QUETCH import writeOutputToFile, f1_ok_and_bad
import numpy as np
import argparse
import matplotlib.pyplot as plt


def loadParams(paramfile):
    """ Load trained parameters from file, build model and evaluate on test set """
    f = open(paramfile,"r")
    params = cPickle.load(f)
    return params

def testModelOnTest_task2_14(languagePair, sourceText, targetText, paramfile, targetWindowSize, sourceWindowSize, wordDictionary=None, featureIndices=None, alignments=None, lowercase=True, full=False, multi=False, acti=None):
    """ Test given model on task 2 data """
    languagePair2 = languagePair.upper().replace("-","_")

#    (x_test,y_test),targetWords_test = t2.get_testtest_xy(task)

    x = T.matrix('x', dtype='int32')  # the data is presented as matrix, one row for each sample
    y = T.ivector('y')  # the labels are presented as vector of [int] labels 
    
    srcf = open("tmp_s", "w")
    srcf.write(sourceText.encode("utf-8"))
    srcf.close()
    tgtf = open("tmp_t","w")
    tgtf.write(targetText.encode("utf-8"))
    tgtf.close()

    testDir = ""
    trainDir = ""

    t2 = WMT14QETask2(languagePair2, testDir, trainDir, targetWindowSize=targetWindowSize, sourceWindowSize=sourceWindowSize, wordDictionary=wordDictionary, featureIndices=featureIndices, alignments=alignments, lowercase=lowercase, multi=multi, demo=True) #no need to specify location of test data since it is reconstructed from dev data location
    (x_dev,y_dev),targetWords_dev  = t2.get_test_xy("bin")

    contextSize = sourceWindowSize+targetWindowSize
    if t2.featureIndices is not None:
        contextSize += targetWindowSize*len(t2.featureIndices)

    params = loadParams(paramfile)
    classifier = NN(None, x, 0, 0, 0, 0, contextSize, params=params, acti=acti)
        
    predict_model_dev= theano.function([], [x, classifier.outputLayer.y_pred, classifier.outputLayer.p_y_given_x, classifier.outputLayer.output], givens={x:x_dev} )
    x_dev,pred_dev,likelihood, out = predict_model_dev()
        
    #print zip(x_dev, pred_dev, likelihood)
    returnString = ""
    for w, pred in zip(targetWords_dev, pred_dev):
        returnString+="%d\t%s\t%s\n" % ( w[1], w[2], t2.intToLabel_bin[pred])
    return returnString

def testModelOnTest_task2_15(languagePair, sourceText, targetText, paramfile, targetWindowSize, sourceWindowSize, wordDictionary=None, featureIndices=None, alignments=None, lowercase=True, full=False, acti=None):
    """ Test given model on task 2 data """

    x = T.matrix('x', dtype='int32')  # the data is presented as matrix, one row for each sample
    y = T.ivector('y')  # the labels are presented as vector of [int] labels 

    x2 = T.matrix('x', dtype='int32')
    y2 = T.ivector('y') 

    srcf = open("tmp_s", "w")
    srcf.write(sourceText.encode("utf-8"))
    srcf.close()
    tgtf = open("tmp_t","w")
    tgtf.write(targetText.encode("utf-8"))
    tgtf.close()

    testDataDir = ""
    trainDataDir = ""

    t2 = WMT15QETask2(languagePair, testDataDir, trainDataDir, wordDictionary=wordDictionary, targetWindowSize=targetWindowSize, sourceWindowSize=sourceWindowSize, featureIndices=featureIndices, alignments=alignments, lowercase=lowercase, full=full, demo=True) #no need to specify location of test data since it is reconstructed from dev data location
    (x_dev,y_dev),targetWords_dev  = t2.get_test_xy(2) 
    contextSize = sourceWindowSize+targetWindowSize
    if t2.featureIndices is not None:
	contextSize += targetWindowSize*len(t2.featureIndices)
		
    params = loadParams(paramfile)
		#self, rng, input, n_hidden, n_out, d_wrd, sizeOfDictionary, contextWindowSize, params=None, acti="tanh"):
    classifier = NN(None, x, 0, 0, 0, 0, contextWindowSize=contextSize, params=params, acti=acti)   
		
    predict_model_dev= theano.function([], [x, classifier.outputLayer.y_pred, classifier.outputLayer.p_y_given_x, classifier.outputLayer.output], givens={x:x_dev} )
    x_dev,pred_dev,likelihood, out = predict_model_dev()
    
    returnString = ""
    #print zip(x_dev, pred_dev, likelihood)
    for w, pred in zip(targetWords_dev, pred_dev):
	returnString+="%d\t%s\t%s\n" % ( w[1], w[2], t2.intToLabel_bin[pred])
    return returnString

if __name__ == "__main__":

    paramDict = {} #best trained models
    paramDict["all"] = (14, "params/2015-10-27--11:29:33.195577.267.params", "params/2015-10-27--11:28:57.402482.dict")
    paramDict["en-es"] = (15, "params/2015-10-28--13:23:29.527414.12.params", "params/2015-10-28--13:23:22.861027.dict")
    paramDict["es-en"] = (14, "params/2015-11-07--11:45:48.630485.597.params", "params/2015-11-07--11:44:18.115104.dict")
    paramDict["de-en"] = (14, "params/2015-11-07--11:45:57.296327.54.params", "params/2015-11-07--11:44:30.307978.dict")
    paramDict["en-de"] = (14, "params/2015-11-07--11:45:49.938477.23.params", "params/2015-11-07--11:44:20.178473.dict")
   
    validPairs = paramDict.keys()
    validPairs = ["all", "en-es", "es-en", "de-en", "en-de"]    
    
    #../dicts/2015-09-04--20:39:48.884760.dict ../parameters/2015-09-04--20:39:52.042938.15.params

    parser = argparse.ArgumentParser(description="QUETCH model evaluation")
    parser.add_argument("LanguagePair", type=str, choices=validPairs, help="Source and target language")
    parser.add_argument("SourceText", type=str)
    parser.add_argument("TargetText", type=str)
    args = parser.parse_args()
    
    languagePair = args.LanguagePair
    sourceText = args.SourceText.decode("utf-8")
    targetText = args.TargetText.decode("utf-8")
    sourceWindowSize = 3
    targetWindowSize = 3
    s2tAlignments = False
    lowerCase = True
    full = True
    multi = True
    acti = "tanh"
    
    year, parameterFile, wdict = paramDict[languagePair]
    
    #en-es: There is clapping, laughter.
    #Hay     OK     de      Part_of_speech  Fluency BAD aplaudir        Part_of_speech  Fluency BAD,       OK      OK      OK, risas   OK      OK      OK 
    #Allí    OK      OK      OK aplaude OK      OK      OK ,       OK      OK      OK risas   OK      OK      OK .       O.      

    if year==14:
        languagePair2 = languagePair.upper().replace("-","_")
        result = testModelOnTest_task2_14(languagePair, sourceText, targetText, parameterFile, targetWindowSize, sourceWindowSize, wordDictionary=wdict,  lowercase = lowerCase, full=full, multi=multi, acti=acti)
    elif year==15:
        languagePair2 = languagePair.upper().replace("-","_")
        result = testModelOnTest_task2_15(languagePair, sourceText, targetText, parameterFile, targetWindowSize, sourceWindowSize, wordDictionary=wdict, lowercase = lowerCase, full=full, acti=acti) 
    
    print "QUETCH results"
    print result