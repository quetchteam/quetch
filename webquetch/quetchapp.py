from bottle import run, get, post, request, route, template
import sys

sys.path.append('demo') #add de

from demo import *

defaultPage='''
        <!doctype html>
        <h1>QUETCH</h1>
        <h2>QUality Estimation from scraTCH</h2>
        <h3>How bad is your translation?</h3>
        <form action="/quetch" method="post">
        Source Language: <select name="srcLang">
                    <option value="en" {{!'selected="selected"' if srcLang=="en" else ""}}>English</option>
                    <option value="es" {{!'selected="selected"' if srcLang=="es" else ""}}>Spanish</option>
                    <option value="de" {{!'selected="selected"' if srcLang=="de" else ""}}>German</option>
                </select> 
        Source: <input value="{{src}}" name="source" type="text" style="width:300px" required> <br>
        Target Language: <select name="tgtLang">
                            <option value="en" {{!'selected="selected"' if tgtLang=="en" else ""}} >English</option>
                                                <option value="es" {{!'selected="selected"' if tgtLang=="es" else ""}}>Spanish</option>
                                                                    <option value="de" {{!'selected="selected"' if tgtLang=="de" else ""}} >German</option>
                                                                                    </select> 

                         
                         Target: <input value="{{tgt}}" name="target" type="text" style="width:300px" required /> <br>
                                            <input value="QUETCH" type="submit" />
                                                    </form>
    '''


@get('/quetch')
def datainput():
    return template(defaultPage, src="", tgt="", srcLang="en", tgtLang="es")

@post('/quetch')
def runquetch():

    #TODO
    #- change models to ap (highest acc), not apw



    paramDict = {} #best trained models (F1)
    paramDict["all"] = (14, "demo/params/2015-10-27--11:29:33.195577.267.params", "demo/params/2015-10-27--11:28:57.402482.dict")
    paramDict["en-es"] = (15, "demo/params/2015-10-28--13:23:29.527414.12.params", "demo/params/2015-10-28--13:23:22.861027.dict")
    #paramDict["es-en"] = (14, "demo/params/2015-11-07--11:45:48.630485.597.params", "demo/params/2015-11-07--11:44:18.115104.dict")
    #paramDict["de-en"] = (14, "demo/params/2015-11-07--11:45:57.296327.54.params", "demo/params/2015-11-07--11:44:30.307978.dict")
    #paramDict["en-de"] = (14, "demo/params/2015-11-07--11:45:49.938477.23.params", "demo/params/2015-11-07--11:44:20.178473.dict")
    paramDict["es-en"] = (14, "demo/params/2015-11-10--00:07:44.645262.370.params", "demo/params/2015-11-10--00:04:55.774515.dict")
    paramDict["de-en"] = (14, "demo/params/2015-11-10--00:15:57.585029.215.params", "demo/params/2015-11-10--00:14:29.707729.dict")
    paramDict["en-de"] = (14, "demo/params/2015-11-10--00:15:47.873218.560.params", "demo/params/2015-11-10--00:14:20.885712.dict")

    validPairs = paramDict.keys()

    source = request.forms.get('source').decode("utf-8")
    target = request.forms.get('target').decode("utf-8")
    srcLang = request.forms.get('srcLang')
    tgtLang = request.forms.get('tgtLang')
    languagePair = srcLang+"-"+tgtLang
    sourceWindowSize = 3
    targetWindowSize = 3
    s2tAlignments = False
    lowerCase = True
    full = True
    multi = True
    acti = "tanh"
    if languagePair not in validPairs:
        return defaultPage+"<br><b> Not a valid language pair. Try again! </b>"
    else:
        year, parameterFile, wdict = paramDict[languagePair]

        if year==14:
            result = testModelOnTest_task2_14(languagePair, source, target, parameterFile, targetWindowSize, sourceWindowSize, wordDictionary=wdict,  lowercase = lowerCase, full=full, multi=multi, acti=acti)
        elif year==15:
            result = testModelOnTest_task2_15(languagePair, source, target, parameterFile, targetWindowSize, sourceWindowSize, wordDictionary=wdict, lowercase = lowerCase, full=full, acti=acti)

        result = result.replace("\n", "<br>")
        return template(defaultPage, src=source, tgt=target, srcLang=srcLang, tgtLang=tgtLang)+template("<br>Quality Estimation for {{languagePair}}: </b><br><br> {{! result}}", languagePair=languagePair, result=result)


run(host='localhost', port=8080, debug=True)

